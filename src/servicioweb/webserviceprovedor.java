package servicioweb;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.jws.WebMethod;
import javax.jws.WebParam;

import org.orm.PersistentException;
import org.postgresql.translation.messages_bg;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;

import capanegocio.*;

public class webserviceprovedor {
/**
 * Metodo implementado con Gson que realiza la busqueda simple
 * @param busqueda
 * @return
 * @throws PersistentException
 */
	@WebMethod(operationName = "busquedaSimpleContacto")
	public String busquedaSimpleContacto(
			@WebParam(name = "busqueda") String busqueda)
			throws PersistentException {
	
		Contacto objContacto = new Contacto();
		Gson Json = new GsonBuilder().create();
		List<Contacto> listDeC = new ArrayList<Contacto>();

		try {
			listDeC = objContacto.busquedaSimple(busqueda);
			if (listDeC.isEmpty()) {
				return "no se encontraron datosss";
			} else {
				return Json.toJson(listDeC);
			}
		} catch (PersistentException e) {
			return e.getMessage();
		}

	}
/**
 * Metodo implementado con Gson que realiza la busqueda Avanzada
 * @return
 * @throws PersistentException
 */
	@WebMethod(operationName = "busquedaAvanzada")
	public String busquedaAvanzada(String busquedaAvanzada)throws PersistentException{
		
		String resultado = "";
		List<Contacto> listarContactos = new ArrayList<Contacto>();
		Contacto contacto = new Contacto();
		Gson listarJson =new Gson();
		try{
			 contacto=listarJson.fromJson(busquedaAvanzada, Contacto.class);
			
		}catch(JsonSyntaxException e){
			return null;
		}
	
		if(contacto.getRun()!= null){
			contacto.setRun(contacto.getRun());
		}			
		else{
			contacto.setRun("");
		}
		
		if(contacto.getNombre() != null){
			contacto.setNombre(contacto.getNombre());
		}else{
			contacto.setNombre("");
		}
		
		if(contacto.getApellido() != null){
			contacto.setApellido(contacto.getApellido());
		}else{
			contacto.setApellido("");
		}
		
		if(contacto.getMail() != null){
			contacto.setMail(contacto.getMail());
		}else{
			contacto.setMail("");
		}
		
		if(contacto.getTelefono()!= null){
			contacto.setTelefono(contacto.getTelefono());
		}else{
			contacto.setTelefono("");
		}
		
		if(contacto.getPais() != null){
			contacto.setPais(contacto.getPais());
		}else{
			contacto.setPais("");
		}
		
		if(contacto.getRegion() != null){
			contacto.setRegion(contacto.getRegion());
		}else{
			contacto.setRegion("");
		}
		if(contacto.getCiudad()!= null){
			contacto.setCiudad(contacto.getCiudad());
		}else{
			contacto.setCiudad("");
		}
		
	
		
		 listarJson = new GsonBuilder().create();
		
		try{
			listarContactos = contacto.busquedaAvanzada(contacto);
			if(listarContactos.isEmpty()){
				resultado = "no se encontraron datos";
			}else{
				resultado = listarJson.toJson(listarContactos);
			}			
		}catch(PersistentException p){
			resultado = p.getMessage();
		}
		return resultado;
	}
	
	@WebMethod(operationName = "busquedaAvanzada2")
	public String busquedaAvanzada2(@WebParam(name = "run") String run,
			@WebParam(name = "nombre") String nombre,
			@WebParam(name = "apellido") String apellido,
			@WebParam(name = "mail") String mail,
			@WebParam(name = "telefono") String telefono,
			@WebParam(name = "pais") String pais,
			@WebParam(name = "region") String region,
			@WebParam(name = "ciudad") String ciudad)
			throws PersistentException {

		List<Contacto> listDeC = new ArrayList<Contacto>();

		Contacto contacto = new Contacto();
		if (run != null) {
			contacto.setRun(run);
		} else {
			contacto.setRun("");
		}
		if (nombre != null) {
			contacto.setNombre(nombre);
		} else {
			contacto.setNombre("");
		}

		if (apellido != null) {
			contacto.setApellido(apellido);
		} else {
			contacto.setApellido("");
		}

		if (mail != null) {
			contacto.setMail(mail);
		} else {
			contacto.setMail("");
		}

		if (telefono != null) {
			contacto.setTelefono(telefono);
		} else {
			contacto.setTelefono("");
		}

		if (pais != null) {
			contacto.setPais(pais);
		} else {
			contacto.setPais("");
		}

		if (region != null) {
			contacto.setRegion(region);
		} else {
			contacto.setRegion("");
		}

		if (ciudad != null) {
			contacto.setCiudad(ciudad);
		} else {
			contacto.setCiudad("");
		}

		Gson Json = new GsonBuilder().create();
		try {
			listDeC = contacto.busquedaAvanzada(contacto);
			if (listDeC.isEmpty()) {
				return "no se encontraron datos";
			} else {
				return Json.toJson(listDeC);
			}
		} catch (PersistentException e) {
			return e.getMessage();
		}

	}
	@WebMethod(operationName = "comprobarU")
	public String comprobarU(
			@WebParam(name = "usuario") String usuario,
			@WebParam(name = "password") String password) throws PersistentException{
		
		Usuario usuarioOB = new Usuario();
		String valido = "a";
		
		if(usuario != null){
			usuarioOB.setUsuario(usuario);
		}			
		else{
			usuarioOB.setUsuario("");
		}
		
		if(password != null){
			usuarioOB.setPassword(password);
		}			
		else{
			usuarioOB.setPassword("");
		}
		
		
		if(usuarioOB.validarU(usuarioOB)){
			valido = "true";
		}else{
			valido = "false";
		}
		
		return valido;
	}
	
	
	@WebMethod(operationName = "verPerfil")
	public String verPerfil(@WebParam(name = "VP") String VP){
		int id=Integer.parseInt(VP);
		try {
			Contacto contactoNegocio=Contacto.verPerfilContacto(id);
			Gson listarJson = new GsonBuilder().create();
			return listarJson.toJson(contactoNegocio);
		} catch (PersistentException e) {
		
			e.printStackTrace();
			return "error";
		}
		
	}
}



