<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link
	href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css"
	rel="stylesheet"></link>
<link
	href="//oss.maxcdn.com/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css"
	rel="stylesheet"></link>

<script src="//oss.maxcdn.com/jquery/1.11.1/jquery.min.js"></script>
<script
	src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script
	src="//oss.maxcdn.com/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.min.js"></script>

<script type="text/javascript" src="validador.js"></script>
<jsp:include page="FormularioMenu.jsp" />
<title>Eliminar Contacto</title>
</head>
<%
	String uid6 = request.getParameter("id");
%>
<body>
	<div class="page-header">
		<h1>
			Eliminar Contacto <small>Rellenar datos</small>
		</h1>
	</div>

	<form action="ServletEliminarCC" id="ServletEliminarCC" method="post"
		class="form-horizontal mitad" action="#">

		<div class="form-group">
			<label class="col-lg-3 control-label">Esta seguro de eliminar
				este contacto</label>
			<div class="col-lg-3">
				<input style="visibility: hidden" type="text" class="form-control"
					name="id" value="<%=uid6%>" required> <br>
			</div>
		</div>

		<div class="form-group">
			<div class="col-lg-9 col-lg-offset-3 ">
				<a href="javascript:window.history.back();" button type="submit"
					class="btn btn-success "">No
					</button>
				</a>
				<button type="submit" class="btn btn-success">Si</button>

			</div>
		</div>


	</form>
</body>
</html>