
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link
	href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css"
	rel="stylesheet"></link>
<link
	href="//oss.maxcdn.com/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css"
	rel="stylesheet"></link>
<script src="//oss.maxcdn.com/jquery/1.11.1/jquery.min.js"></script>
<script
	src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script
	src="//oss.maxcdn.com/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.min.js"></script>
<script type="text/javascript" src="./js/validador.js"></script>
<script type="text/javascript" src="js/image.js"></script>
<jsp:include page="FormularioMenu.jsp" />
<title>Actualizar Contacto</title>
</head>
<%
	String uid3 = request.getParameter("id");
%>
<%@ page import="capanegocio.*"%>
<%@ page import="java.util.*"%>

<body>
	<div class="page-header">
		<h1>
			Actualizar Contacto <small>Rellenar datos</small>
		</h1>
	</div>

	<form action="ServletActualizarCC" id="ServletActualizarCC"
		method="post" class="form-horizontal mitad">


		<div class="form-group">
			<label class="col-lg-3 control-label">Run</label>
			<div class="col-lg-3">
				<input type="text" class="form-control" id="run" name="run"
					value="${run}" readonly> <br>
			</div>
		</div>
		<div class="form-group">
			<label class="col-lg-3 control-label">Nombre</label>
			<div class="col-lg-3">
				<input type="text" class="form-control" id="nombre" name="nombre"
					value="${nombre}" required> <br>
			</div>
		</div>

		<div class="form-group">
			<label class="col-lg-3 control-label">Apellido</label>
			<div class="col-lg-3">
				<input type="text" class="form-control"
					placeholder="Ingrese Apellido" name="apellido" value="${apellido}"
					required> <br>
			</div>
		</div>

		<div class="form-group">
			<label class="col-lg-3 control-label">Mail</label>
			<div class="col-lg-3">
				<input type="text" class="form-control" placeholder="Ingrese Mail"
					value="${mail}" name="mail" required> <br>
			</div>
		</div>

		<div class="form-group">
			<label class="col-lg-3 control-label">Telefono</label>
			<div class="col-lg-3">
				<input type="text" class="form-control"
					placeholder="Ingrese Telefono" name="telefono" value="${telefono}"
					required> <br>
			</div>
		</div>
		<div class="form-group">
			<label class="col-lg-3 control-label">Ciudad</label>
			<div class="col-lg-3">
				<input type="text" class="form-control" placeholder="Ingrese Ciudad"
					name="ciudad" value="${ciudad}" required> <br>
			</div>
		</div>


		<div class="form-group">
			<label class="col-lg-3 control-label">Region</label>
			<div class="col-lg-3">
				<input type="text" class="form-control" placeholder="Ingrese Region"
					name="region" value="${region}" required> <br>
			</div>
		</div>

		<div class="form-group">
			<label class="col-lg-3 control-label">Pais</label>
			<div class="col-lg-3">
				<input type="text" class="form-control" placeholder="Ingrese Pais"
					name="pais" value="${pais}" required> <br>
			</div>
		</div>




		<div class="form-group">
			<label for="foto">Foto Contacto:</label> <input id="inputImagen"
				name="inputImagen" type="file" multiple accept='image/*'
				onchange="encodeImage();" />
		</div>
		<div class="form-group">
			<textarea id="foto" name="foto" class="form-control textbox"
				style="display: none;"></textarea>
		</div>

		<div class="form-group" id="imgContainer"></div>

		<div class="form-group">
			<label class="col-lg-3 control-label">Empresa</label>
			<div class="col-lg-3">
				<select class="form-control" name="empresa" id="empresa">

					<option value="1">Pcfactory</option>

				</select> <br>
			</div>
		</div>



		<div class="form-group">
			<div class="col-lg-9 col-lg-offset-3">
				<button type="submit" class="btn btn-success left">Enviar</button>
			</div>
		</div>

		<div class="form-group">
			<label class="col-lg-3 control-label"></label>
			<div class="col-lg-3">
				<input style="visibility: hidden" type="text" class="form-control"
					name="uid3" value="<%=uid3%>" required> <br>
			</div>
		</div>
	</form>
</body>
</html>