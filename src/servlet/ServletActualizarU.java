package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.orm.PersistentException;
import org.orm.PersistentTransaction;

import servlet.ServletActualizarCC;
import capanegocio.Contacto;
import capanegocio.Usuario;

/**
 * Servlet implementation class ServletActualizarU
 */
public class ServletActualizarU extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletActualizarU() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	/**
	 * doPost Permite 
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PersistentTransaction t = null;
		PrintWriter out= response.getWriter();
		String m="";
		try{
			int id= Integer.parseInt(request.getParameter("id"));
			String user= request.getParameter("user");
			String pass= request.getParameter("pass");
			
			ServletActualizarU refrescar = new ServletActualizarU();
			refrescar.validarId(id);
			
			Usuario actualizar = new Usuario();
			
			if(id < 0 || user.trim().equals("")|| pass.trim().equals("")){
				System.out.println("variable vacia");	
			} else {	
				if (user.length() <=20 && pass.length() <=20){
					
					m=" Actualizacion en la iddd: "+ id+ " Realizada";
					out.println(" Actualizacion en la id: "+ id+ " Realizada");
					actualizar.setUid(id);
					actualizar.setUsuario(user);
					actualizar.setPassword(pass);
					
					try {
						Usuario.actualizar(actualizar);
					} catch (PersistentException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}catch(NullPointerException e){
			e.printStackTrace();
		}
	}

	/**
	 * M�todo que permite validar que el formato de la cadena ingresada sea num�rico
	 * @param cad = cadena ingresada
	 * @return
	 */
	private boolean esEntero(String cad){
		 for(int i = 0; i<cad.length(); i++)
		 if( !Character.isDigit(cad.charAt(i)) ){
			 return false;
		 }
		 return true;
	 }
	
	/**
	 * M�todo que permite validar si el campo ingresado en id es entero
	 * @param id = campo ingresado en id
	 * @return
	 */
	private boolean validarId(int id){
		String cad = String.valueOf(id);
		 for(int i = 0; i<cad.length(); i++)
		 if( !Character.isDigit(cad.charAt(i)) ){
			 return false;
		 }
		 return true;
	 }
	}


