package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.orm.PersistentException;
import org.orm.PersistentTransaction;

import capanegocio.Contacto;
import capanegocio.Empresa;

/**
 * Servlet implementation class ServletActualizarCC
 */
public class ServletActualizarCC extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static final String PATTERN_EMAIL = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
			+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ServletActualizarCC() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	/**
	 * Método que actualiza Contactos
	 */

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PersistentTransaction t = null;
	//	String uid3= request.getParameter("id"); 
	//	String run1= request.getParameter("run"); 
	
		
	
		
	
		PrintWriter out = response.getWriter();
		Empresa oEmpresa = new Empresa();
		Contacto oContacto = new Contacto();
		
		//String	contacto= request.getParameter("uid");
		ServletActualizarCC actualizar = new ServletActualizarCC();
		String mensaje = "";

		try {
			int id = Integer.parseInt(request.getParameter("uid3"));
			String run = request.getParameter("run");
			String nombre = request.getParameter("nombre");
			String apellido = request.getParameter("apellido");
			String mail = request.getParameter("mail");
			String telefono = request.getParameter("telefono");
			String pais = request.getParameter("pais");
			String region = request.getParameter("region");
			String ciudad = request.getParameter("ciudad");
			String foto=request.getParameter("foto");
			String empresa = request.getParameter("empresa");
			int idEmpresa = Integer.parseInt(empresa);

			actualizar.validacionId(id);
			actualizar.validacionRun(run);
			actualizar.validacionEmail(mail);
			actualizar.validacionEntero(telefono);

			if ((actualizar.validacionId(id))
					&& (actualizar.validacionRun(run) == true)
					&& (actualizar.validacionEmail(mail) == true)
					&& (actualizar.validacionEntero(telefono))) {

				if (id < 0 || run.trim().equals("") || nombre.trim().equals("")
						|| apellido.trim().equals("") || mail.trim().equals("")
						|| telefono.trim().equals("") || pais.trim().equals("")
						|| region.trim().equals("") || ciudad.trim().equals("")) {
					System.out.println("variable vacia");

				} else {

					if (run.length() <= 12 && nombre.length() <= 50
							&& apellido.length() <= 50 && mail.length() <= 20
							&& telefono.length() <= 20 && pais.length() <= 20
							&& region.length() <= 20 && ciudad.length() <= 20) {

						oContacto.setUid(id);
						oContacto.setRun(run);
						oContacto.setNombre(nombre);
						oContacto.setApellido(apellido);
						oContacto.setMail(mail);
						oContacto.setTelefono(telefono);
						oContacto.setPais(pais);
						oContacto.setRegion(region);
						oContacto.setCiudad(ciudad);
						oContacto.setFoto(foto);
						oEmpresa.setUid(idEmpresa);
                        oContacto.setEmpresa(oEmpresa);
						

						String r = "";
						try {
							r = Contacto.actualizar(oContacto);
							mensaje = "Actualización Completada";
							RequestDispatcher reD = request
									.getRequestDispatcher("FormularioMenu.jsp");
							request.setAttribute("mensaje", mensaje);
							reD.forward(request, response);
						} catch (PersistentException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			} else {
				mensaje = "Error en la Actualizacion";
				RequestDispatcher reD = request
						.getRequestDispatcher("FormularioActualizarContacto.jsp");
				request.setAttribute("mensaje", mensaje);
				reD.forward(request, response);
			}
		} catch (NullPointerException e) {
			e.printStackTrace();
		}

	}
	
	/**
	 * Metodo que permite validar el mail
	 * @param email
	 * @return boolean
	 */
	private boolean validacionEmail(String email) {

		// Compiles the given regular expression into a pattern.
		Pattern pattern = Pattern.compile(PATTERN_EMAIL);

		// Match the given input against this pattern
		Matcher matcher = pattern.matcher(email);
		return matcher.matches();

	}

	
	/**
	 * Método que permite validar que sea numérico
	 * @param cad
	 * @return boolean
	 */
	private boolean validacionEntero(String cad) {
		for (int i = 0; i < cad.length(); i++)
			if (!Character.isDigit(cad.charAt(i))) {
				return false;
			}
		return true;
	}

	
	/**
	 * Método que permite validar si id entero
	 * @param id
	 * @return boolean
	 */
	private boolean validacionId(int id) {
		String cad = String.valueOf(id);
		for (int i = 0; i < cad.length(); i++)
			if (!Character.isDigit(cad.charAt(i))) {
				return false;
			}
		return true;
	}

	
	
	/**
	 * Método que permite validar el run
	 * @param run
	 * @return boolean
	 */
	public static boolean validacionRun(String run) {

		boolean validacion = false;
		try {
			run = run.toUpperCase();
			run = run.replace(".", "");
			run = run.replace("-", "");
			int runAux = Integer.parseInt(run.substring(0, run.length() - 1));

			char dv = run.charAt(run.length() - 1);

			int m = 0, s = 1;
			for (; runAux != 0; runAux /= 10) {
				s = (s + runAux % 10 * (9 - m++ % 6)) % 11;
			}
			if (dv == (char) (s != 0 ? s + 47 : 75)) {
				validacion = true;
			}

		} catch (java.lang.NumberFormatException e) {
		} catch (Exception e) {
		}
		return validacion;
	}

	/**
	 * @see HttpServlet#doPut(HttpServletRequest, HttpServletResponse)
	 */
	protected void doPut(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doDelete(HttpServletRequest, HttpServletResponse)
	 */
	protected void doDelete(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}