package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class traspaso
 */
public class traspaso extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public traspaso() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		String run = request.getParameter("run");
		String nombre = request.getParameter("nombre");
		String apellido  = request.getParameter("apellido");
		String mail = request.getParameter("mail");
		String telefono = request.getParameter("telefono");
		String ciudad  = request.getParameter("ciudad");
		String region = request.getParameter("region");
		String pais = request.getParameter("pais");
		String foto  = request.getParameter("foto");
		
		RequestDispatcher rs = request.getRequestDispatcher("/FormularioActualizarContacto.jsp");
		
		request.setAttribute("run", run);
		request.setAttribute("nombre", nombre);
		request.setAttribute("apellido", apellido);
		request.setAttribute("mail", mail);
		request.setAttribute("telefono", telefono);
		request.setAttribute("ciudad", ciudad);
		request.setAttribute("region", region);
		request.setAttribute("pais", pais);
		request.setAttribute("foto", foto);
		
		
		
		
		rs.forward(request, response);
	}

}
