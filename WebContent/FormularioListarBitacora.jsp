<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="i"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link
	href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css"
	rel="stylesheet"></link>
<link
	href="//oss.maxcdn.com/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css"
	rel="stylesheet"></link>

<script src="//oss.maxcdn.com/jquery/1.11.1/jquery.min.js"></script>
<script
	src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script
	src="//oss.maxcdn.com/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.min.js"></script>

<jsp:include page="FormularioMenu.jsp" />

<title>listar Contacto con bitacora</title>
<title>Lista Contactos Empresariales</title>
</head>
<body>



	<form action="ServletListarBitacora" method="get">
		<div class="page-header">
			<h1>Lista de Contactos con bitacora</h1>
		</div>
		<%
			try {
				request.getAttribute("bitacora").toString();
		%>
		<table class="table table-bordered table-hover table-responsive">
			<tr class="success">
			<thead>

				<th>Run</th>
				<th>Nombre</th>
				<th>Apellido</th>
				<th>Titulo</th>
				<th>comentario</th>
				<th>foto</th>



			</thead>
			</tr>
			<tbody>
				<i:forEach items="${bitacora}" var="bitacora">
					<tr>

						<td>${bitacora.contacto.run}</td>
						<td>${bitacora.contacto.nombre}</td>
						<td>${bitacora.contacto.apellido}</td>
						<td>${bitacora.titulo}</td>
						<td>${bitacora.comentario}</td>
						<td><img src="${bitacora.contacto.foto}"
							class="img-responsive"></img></td>
					<tr>

						<form action="FormularioQueAyuda.jsp" method="post">
							<input type="hidden" value="Eliminar" class="btn btn-danger">
						</form>

						<td>
							<form action="ServletEliminarBitacora" method="post">
								<input type="hidden" value="${bitacora.uid}" name="uid">
								<input type="submit" value="Eliminar" class="btn btn-danger">
							</form>
						</td>
					</tr>

					</tr>

				</i:forEach>
			</tbody>

			<%
				} catch (NullPointerException e) {
				}
			%>
			<button type="submit" class="btn btn-info">Listar Contactos</button>
		</table>



	</form>
</body>
</html>