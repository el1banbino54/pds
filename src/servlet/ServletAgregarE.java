package servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.orm.PersistentException;
import org.orm.PersistentTransaction;



import capanegocio.Empresa;

/**
 * Servlet implementation class ServletAgregarE
 */
public class ServletAgregarE extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletAgregarE() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out= response.getWriter();
		PersistentTransaction t = null;
		String rut="";
		String nombre= "";
		String direccion= "";
		String telefono="";
		String mail="";
		String ciudad="";
		String region="";
		String pais="";
		String razonSocial="";
		String representanteLegal="";
		String rubro="";
		
		String m="";
		ServletAgregarE ingreso = new ServletAgregarE();
		
		 try{
			    rut= request.getParameter("rut");
				nombre= request.getParameter("nombre");
				telefono= request.getParameter("telefono");
				mail= request.getParameter("mail");
				direccion= request.getParameter("direccion");
				ciudad= request.getParameter("ciudad");
				region= request.getParameter("region");
				pais= request.getParameter("pais");
				razonSocial= request.getParameter("razonSocial");
				representanteLegal= request.getParameter("representanteLegal");
				rubro= request.getParameter("rubro");
				
				if(rut.trim().equals("") ){
					System.out.println("variable vacia");
					
				}else{
					if (rut.length()<=12  ){
						
					Empresa ingresar = new Empresa();

						      ingresar.setRut(rut);
						      ingresar.setNombre(nombre);
						      ingresar.setTelefono(telefono);
						      ingresar.setMail(mail);
						      ingresar.setDireccion(direccion);
						      ingresar.setCiudad(ciudad);
						      ingresar.setRegion(region);
						      ingresar.setPais(pais);
						      ingresar.setRazonSocial(razonSocial);
						      ingresar.setRepresentanteLegal(representanteLegal);
						      ingresar.setRubro(rubro);
					
					    try {
						    Empresa.ingresar(ingresar);
						    m="empresa: "+ nombre+ " Agregado";
						    out.println("empresa: "+ rut+ " Agregado");
					         } 
					     catch (PersistentException e) {
					    	// TODO Auto-generated catch block
						    e.printStackTrace();
					         }
					}
				}
					
		 }catch(NullPointerException e){
			   e.printStackTrace();
				
			}
	}
	}



