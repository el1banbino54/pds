package capanegocio;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Restrictions;
import org.orm.PersistentException;
import org.orm.PersistentTransaction;







import orm.ContactoCriteria;
import servlet.ServletAgregarCC;
import servlet.ServletAgregarU;

public class Contacto {
	public Contacto() {
	}

	private static final int ROW_COUNT = 100;

	private Empresa empresa;
    
	private int uid;

	private String run;

	private String nombre;

	private String apellido;

	private String mail;

	private String telefono;

	private String pais;

	private String region;

	private String ciudad;
	
	private String foto;


	/**
	 * 
	 * @return int Uid de contacto
	 */
	public int getUid() {
		return uid;
	}

	/**
	 * 
	 * @param int uid de contacto
	 */
	public void setUid(int uid) {
		this.uid = uid;
	}

	/**
	 * 
	 * @return String nombre del contacto
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * 
	 * @param String nombre
	 *            del contacto
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * 
	 * @return String apellido del contacto
	 */
	public String getApellido() {
		return apellido;
	}

	/**
	 * 
	 * @param String
	 *            apellido del contacto
	 */
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	/**
	 * 
	 * @return String mail del contacto
	 */
	public String getMail() {
		return mail;
	}

	/**
	 * 
	 * @param String
	 *            mail del contacto
	 */
	public void setMail(String mail) {
		this.mail = mail;
	}

	/**
	 * 
	 * @return String telefono del contacto
	 */
	public String getTelefono() {
		return telefono;
	}

	/**
	 * 
	 * @param String
	 *            telefono del contacto
	 */
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	/**
	 * 
	 * @return String pais del contacto
	 */
	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	/**
	 * 
	 * @return String region del contacto
	 */
	public String getRegion() {
		return region;
	}

	/**
	 * 
	 * @param String
	 *            region del contacto
	 */
	public void setRegion(String region) {
		this.region = region;
	}

	/**
	 * 
	 * @return String ciudad de contacto
	 */
	public String getCiudad() {
		return ciudad;
	}

	/**
	 * 
	 * @param String
	 *            ciudad de contacto
	 */
	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	/**
	 * 
	 * @return String run de contacto
	 */
	public String getRun() {
		return run;
	}

	/**
	 * 
	 * @param String
	 *            run de contacto
	 */
	public void setRun(String run) {
		this.run = run;
	}

	
	
	
	public String getFoto() {
		return foto;
	}

	public void setFoto(String foto) {
		this.foto = foto;
	}

	/**
	 * 
	 * @return Empresa de empresa
	 */
	public Empresa getEmpresa() {
		return empresa;
	}

	/**
	 * 
	 * @param Empresa
	 *            de empresa
	 */
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}



	

	

	

	/**
	 * M�todo ingresar permite registrar un contacto
	 * 
	 * @param contacto
	 * @return retorna un String que en este caso seria la variable mensaje
	 * @throws PersistentException
	 */
	public static String ingresar(Contacto contacto) throws PersistentException {
		PersistentTransaction t = orm.Taller1MagisterInformaticaPersistentManager
				.instance().getSession().beginTransaction();
		String mensaje = "contacto no ingresado";
		try {
			orm.Contacto lormContacto = orm.ContactoDAO.createContacto();
			orm.Empresa lormEmpresa = orm.EmpresaDAO.loadEmpresaByORMID(contacto.getEmpresa().getUid());
			
			// Initialize the properties of the persistent object here

			try {
				lormContacto.setRun(contacto.getRun());
			} catch (NullPointerException e) {
				e.printStackTrace();
			}
			try {
				lormContacto.setNombre(contacto.getNombre());
			} catch (NullPointerException e) {
				e.printStackTrace();
			}
			try {
				lormContacto.setApellido(contacto.getApellido());
			} catch (NullPointerException e) {
				e.printStackTrace();
			}

			try {
				lormContacto.setMail(contacto.getMail());
			} catch (NullPointerException e) {
				e.printStackTrace();
			}
			try {
				lormContacto.setTelefono(contacto.getTelefono());
			} catch (NullPointerException e) {
				e.printStackTrace();
			}
			try {
				lormContacto.setPais(contacto.getPais());
			} catch (NullPointerException e) {
				e.printStackTrace();
			}
			try {
				lormContacto.setRegion(contacto.getRegion());
			} catch (NullPointerException e) {
				e.printStackTrace();
			}
			try {
				lormContacto.setCiudad(contacto.getCiudad());
			} catch (NullPointerException e) {
				e.printStackTrace();
			}
			try {
				lormContacto.setEmpresaU(lormEmpresa);
			} catch (NullPointerException e) {
				e.printStackTrace();
			}
			lormContacto.setFoto(contacto.getFoto());
			
			
			orm.ContactoDAO.save(lormContacto);
			t.commit();
			mensaje = "contacto  ingresado";
		} catch (Exception e) {
			t.rollback();
		}
		return mensaje;

	}

	/**
	 * M�todo actualizar permite actualizar un contacto
	 * 
	 * @param contacto
	 * @return retorna un String en este caso la variable mensaje
	 * @throws PersistentException
	 */
	
	public static String actualizar(Contacto contacto)
			throws PersistentException {
		PersistentTransaction t = orm.Taller1MagisterInformaticaPersistentManager
				.instance().getSession().beginTransaction();
		String mensaje = "no Actualizado";
		try {
			orm.Contacto lormContacto = orm.ContactoDAO
					.loadContactoByORMID(contacto.getUid());
			
			// Update the properties of the persistent object
			orm.Empresa lormEmpresa = orm.EmpresaDAO
					.loadEmpresaByORMID(contacto.getEmpresa().getUid());
		
		
			try {
				lormContacto.setRun(contacto.getRun());
			} catch (NullPointerException e) {
				e.printStackTrace();
			}
			try {
				lormContacto.setNombre(contacto.getNombre());
			} catch (NullPointerException e) {
				e.printStackTrace();
			}
			try {
				lormContacto.setApellido(contacto.getApellido());
			} catch (NullPointerException e) {
				e.printStackTrace();
			}
			try {
				lormContacto.setMail(contacto.getMail());
			} catch (NullPointerException e) {
				e.printStackTrace();
			}
			try {
				lormContacto.setTelefono(contacto.getTelefono());
			} catch (NullPointerException e) {
				e.printStackTrace();
			}
			try {
				lormContacto.setPais(contacto.getPais());
			} catch (NullPointerException e) {
				e.printStackTrace();
			}
			try {
				lormContacto.setRegion(contacto.getRegion());
			} catch (NullPointerException e) {
				e.printStackTrace();
			}

			try {
				lormContacto.setCiudad(contacto.getCiudad());
			} catch (NullPointerException e) {
				e.printStackTrace();
			}

			lormContacto.setFoto(contacto.getFoto());
			
			lormContacto.setEmpresaU(lormEmpresa);
			mensaje = "Actualizacion exitosa";

			orm.ContactoDAO.save(lormContacto);
			t.commit();

		} catch (Exception e) {
			t.rollback();
		}
		return mensaje;

	}

	/**
	 * M�todo borrar permite eliminar un contacto
	 * 
	 * @param contacto
	 * @return Retorna un String en este caso la variable mensaje
	 * @throws PersistentException
	 */
	public static String borrar(Contacto contacto) throws PersistentException {
		PersistentTransaction t = orm.Taller1MagisterInformaticaPersistentManager
		.instance().getSession().beginTransaction();
		String mensaje = "no borrado";
		try {
			orm.Contacto lormContacto = orm.ContactoDAO
					.loadContactoByORMID(contacto.uid);
			
			
			// Delete the persistent object
			orm.ContactoDAO.delete(lormContacto);
		
			t.commit();
			mensaje = "borrado";
		} catch (Exception e) {
			t.rollback();
		}
		return mensaje;
	}

	/**
	 * M�todo listar() que lista los contactos almacenados en la BD @
	 * 
	 * @return retorna una lista de contactos
	 * @throws PersistentException
	 */

	public static List<Contacto> listar() throws PersistentException {

		List<orm.Contacto> listaContactoOrm = orm.ContactoDAO.queryContacto(
				null, null);
		List<Contacto> listaContacto = new ArrayList<>();

		for (orm.Contacto contactoOrm : listaContactoOrm) {
			Contacto contactoNegocio = new Contacto();
			Empresa empresaNegocio = new Empresa();
			orm.Empresa empresaOrm = orm.EmpresaDAO
					.loadEmpresaByORMID(contactoOrm.getEmpresaU().getUid());

			empresaNegocio.setUid(empresaOrm.getUid());
			empresaNegocio.setRut(empresaOrm.getRut());
			empresaNegocio.setNombre(empresaOrm.getNombre());
			empresaNegocio.setTelefono(empresaOrm.getTelefono());
			empresaNegocio.setMail(empresaOrm.getMail());
			empresaNegocio.setCiudad(empresaOrm.getCiudad());
			empresaNegocio.setRegion(empresaOrm.getRegion());
			empresaNegocio.setPais(empresaOrm.getNombre());
			empresaNegocio.setRazonSocial(empresaOrm.getRazonSocial());
			empresaNegocio.setRepresentanteLegal(empresaOrm
					.getRepresentanteLegal());
			empresaNegocio.setRubro(empresaOrm.getRubro());

			contactoNegocio.setEmpresa(empresaNegocio);

			contactoNegocio.setUid(contactoOrm.getUid());
			contactoNegocio.setRun(contactoOrm.getRun());
			contactoNegocio.setNombre(contactoOrm.getNombre());
			contactoNegocio.setApellido(contactoOrm.getApellido());
			contactoNegocio.setTelefono(contactoOrm.getTelefono());
			contactoNegocio.setMail(contactoOrm.getMail());
			contactoNegocio.setCiudad(contactoOrm.getCiudad());
			contactoNegocio.setRegion(contactoOrm.getRegion());
			contactoNegocio.setPais(contactoOrm.getPais());
		
            contactoNegocio.setFoto(contactoOrm.getFoto()); 
			
			listaContacto.add(contactoNegocio);
			
		}

		return listaContacto;
	}

	/**
	 * M�todo que permite hacer un busqueda simple de contactos
	 * 
	 * @param busqueda
	 * @return retorna una lista de contactos
	 * @throws PersistentException
	 */

	public List<Contacto> busquedaSimple(String busqueda)
			throws PersistentException {

		List<Contacto> listaContacto = new ArrayList<Contacto>();
		List<orm.Contacto> listaContactos = new ArrayList<orm.Contacto>();

		if (busqueda != null || !busqueda.equals("")) {
		
			listaContactos = orm.ContactoDAO.queryContacto("Contacto.run='"
					+ busqueda 
					+ "' OR Contacto.nombre='" + busqueda
					+ "' OR Contacto.apellido='" + busqueda
					+ "' OR Contacto.telefono='" + busqueda
					+ "' OR Contacto.mail='" + busqueda
					+ "' OR Contacto.ciudad='" + busqueda
					+ "' OR Contacto.region='" + busqueda
					+ "' OR Contacto.pais='" + busqueda + "'  ", null);
		}

		ContactoCriteria cc= new ContactoCriteria();
		Criterion run= Restrictions.ilike("run", busqueda.toLowerCase());
		Criterion nombre= Restrictions.ilike("nombre", busqueda.toLowerCase());
		Criterion apellido= Restrictions.ilike("apellido", busqueda.toLowerCase());
		Criterion telefono= Restrictions.ilike("telefono", busqueda.toLowerCase());
		Criterion mail= Restrictions.ilike("mail", busqueda.toLowerCase());
		Criterion ciudad= Restrictions.ilike("ciudad", busqueda.toLowerCase());
		Criterion region= Restrictions.ilike("region", busqueda.toLowerCase());
		Criterion pais= Restrictions.ilike("pais", busqueda.toLowerCase());
		
		Disjunction o=Restrictions.or(run,nombre,apellido,telefono,mail,ciudad,region,pais);
        cc.add(o);
        listaContactos=Arrays.asList(orm.ContactoDAO.listContactoByCriteria(cc));
		
		if (listaContactos != null) {
			
			for (orm.Contacto contactoOrm : listaContactos) {
				Contacto contactoNegocio = new Contacto();
				Empresa empresaNegocio = new Empresa();
				orm.Empresa empresaOrm = orm.EmpresaDAO
						.loadEmpresaByORMID(contactoOrm.getEmpresaU().getUid());
																				

				empresaNegocio.setUid(empresaOrm.getUid());
				empresaNegocio.setRut(empresaOrm.getRut());
				empresaNegocio.setNombre(empresaOrm.getNombre());
				empresaNegocio.setTelefono(empresaOrm.getTelefono());
				empresaNegocio.setMail(empresaOrm.getMail());
				empresaNegocio.setCiudad(empresaOrm.getCiudad());
				empresaNegocio.setRegion(empresaOrm.getRegion());
				empresaNegocio.setPais(empresaOrm.getNombre());
				empresaNegocio.setRazonSocial(empresaOrm.getRazonSocial());
				empresaNegocio.setRepresentanteLegal(empresaOrm
						.getRepresentanteLegal());
				empresaNegocio.setRubro(empresaOrm.getRubro());

				contactoNegocio.setEmpresa(empresaNegocio);

				contactoNegocio.setUid(contactoOrm.getUid());
				contactoNegocio.setRun(contactoOrm.getRun());
				contactoNegocio.setNombre(contactoOrm.getNombre());
				contactoNegocio.setApellido(contactoOrm.getApellido());
				contactoNegocio.setTelefono(contactoOrm.getTelefono());
				contactoNegocio.setMail(contactoOrm.getMail());
				contactoNegocio.setCiudad(contactoOrm.getCiudad());
				contactoNegocio.setRegion(contactoOrm.getRegion());
				contactoNegocio.setPais(contactoOrm.getPais());
			
				contactoNegocio.setFoto(contactoOrm.getFoto());

				listaContacto.add(contactoNegocio);
			}
		}
		return listaContacto;
	}

public static Contacto verPerfilContacto(int idC) throws PersistentException{
		
		Contacto contacto=new Contacto();
	    orm.Contacto contactoOrm = orm.ContactoDAO.loadContactoByORMID(idC);
	   
	    contacto.setUid(contactoOrm.getUid());
        contacto.setRun(contactoOrm.getRun());
        contacto.setNombre(contactoOrm.getNombre());
        contacto.setApellido(contactoOrm.getApellido());
        contacto.setMail(contactoOrm.getMail());
        contacto.setTelefono(contactoOrm.getTelefono());
        contacto.setPais(contactoOrm.getPais());
        contacto.setRegion(contactoOrm.getRegion());
        contacto.setCiudad(contactoOrm.getCiudad());
        contacto.setFoto(contactoOrm.getFoto());
	    
	    return contacto;
	}
	

	
	/**
	 * metodo que permite buscar un contacto de manera avanzada
	 * 
	 * @param contacto
	 * @return lista de contactos
	 * @throws PersistentException
	 */

	public List<Contacto> busquedaAvanzada(Contacto contacto)
			throws PersistentException {
		List<Contacto> listaContacto = new ArrayList<Contacto>();
		List<orm.Contacto> listaContactos = new ArrayList<orm.Contacto>();
		String query = "";

		ContactoCriteria cc= new ContactoCriteria();
				
		if (contacto.getRun() != null && !contacto.getRun().trim().equals("")) {
			 cc.add(Restrictions.ilike("run", contacto.run.toLowerCase()));
			query += "Contacto.run='" + contacto.getRun() + "' ";
			
			
		}
/*
		if ((contacto.getRun() != null && !contacto.getRun().trim().equals(""))
				&& (contacto.getNombre() != null && !contacto.getNombre()
						.trim().equals(""))) {
			query += "and ";
		}
	*/
		if (contacto.getNombre() != null
				&& !contacto.getNombre().trim().equals("")) {
			 cc.add(Restrictions.ilike("nombre", contacto.nombre.toLowerCase()));
			query += "Contacto.nombre='" + contacto.getNombre() + "' ";
		}
/*
		if (((contacto.getRun() != null && !contacto.getRun().trim().equals("")) || (contacto
				.getNombre() != null && !contacto.getNombre().trim().equals("")))
				&& (contacto.getApellido() != null && !contacto.getApellido()
						.trim().equals(""))) {
			query += "and ";
		}

	*/	if (contacto.getApellido() != null
				&& !contacto.getApellido().trim().equals("")) {
			 cc.add(Restrictions.ilike("apellido", contacto.apellido.toLowerCase()));
			query += "Contacto.apellido='" + contacto.getApellido() + "' ";
		}
/*
		if ((((contacto.getRun() != null && !contacto.getRun().trim()
				.equals("")) || (contacto.getNombre() != null && !contacto
				.getNombre().trim().equals(""))) || (contacto.getApellido() != null && !contacto
				.getApellido().trim().equals("")))
				&& (contacto.getMail() != null && !contacto.getMail().trim()
						.equals(""))) {
			query += "and ";
		}
*/
		if (contacto.getMail() != null && !contacto.getMail().trim().equals("")) {
			 cc.add(Restrictions.ilike("mail", contacto.mail.toLowerCase()));
			query += "Contacto.mail='" + contacto.getMail() + "' ";
		}
/*
		if (((((contacto.getRun() != null && !contacto.getRun().trim()
				.equals("")) || (contacto.getNombre() != null && !contacto
				.getNombre().trim().equals(""))) || (contacto.getApellido() != null && !contacto
				.getApellido().trim().equals(""))) || (contacto.getMail() != null && !contacto
				.getMail().trim().equals("")))
				&& (contacto.getTelefono() != null && !contacto.getTelefono()
						.trim().equals(""))) {
			query += "and ";
		}*/
		if (contacto.getTelefono() != null
				&& !contacto.getTelefono().trim().equals("")) {
			 cc.add(Restrictions.ilike("telefono", contacto.telefono.toLowerCase()));
			query += "Contacto.telefono='" + contacto.getTelefono() + "' ";
		}
/*
		if ((((((contacto.getRun() != null && !contacto.getRun().trim()
				.equals("")) || (contacto.getNombre() != null && !contacto
				.getNombre().trim().equals(""))) || (contacto.getApellido() != null && !contacto
				.getApellido().trim().equals(""))) || (contacto.getMail() != null && !contacto
				.getMail().trim().equals(""))) || (contacto.getTelefono() != null && !contacto
				.getTelefono().trim().equals("")))
				&& (contacto.getRegion() != null && !contacto.getRegion()
						.trim().equals(""))) {
			query += "and ";
		}*/
		if (contacto.getRegion() != null
				&& !contacto.getRegion().trim().equals("")) {
			 cc.add(Restrictions.ilike("region", contacto.region.toLowerCase()));
			query += "Contacto.region='" + contacto.getRegion() + "' ";
		}
/*
		if (((((((contacto.getRun() != null && !contacto.getRun().trim()
				.equals("")) || (contacto.getNombre() != null && !contacto
				.getNombre().trim().equals(""))) || (contacto.getApellido() != null && !contacto
				.getApellido().trim().equals(""))) || (contacto.getMail() != null && !contacto
				.getMail().trim().equals(""))) || (contacto.getTelefono() != null && !contacto
				.getTelefono().trim().equals(""))) || (contacto.getRegion() != null && !contacto
				.getRegion().trim().equals("")))
				&& (contacto.getCiudad() != null && !contacto.getCiudad()
						.trim().equals(""))) {
			query += "and ";
		}
		*/
		if (contacto.getCiudad() != null
				&& !contacto.getCiudad().trim().equals("")) {
			 cc.add(Restrictions.ilike("ciudad", contacto.ciudad.toLowerCase()));
			query += "Contacto.ciudad='" + contacto.getCiudad() + "' ";
		}
/*
		if ((((((((contacto.getRun() != null && !contacto.getRun().trim()
				.equals("")) || (contacto.getNombre() != null && !contacto
				.getNombre().trim().equals(""))) || (contacto.getApellido() != null && !contacto
				.getApellido().trim().equals(""))) || (contacto.getMail() != null && !contacto
				.getMail().trim().equals(""))) || (contacto.getTelefono() != null && !contacto
				.getTelefono().trim().equals(""))) || (contacto.getRegion() != null && !contacto
				.getRegion().trim().equals(""))) || (contacto.getCiudad() != null && !contacto
				.getCiudad().trim().equals("")))
				&& (contacto.getPais() != null && !contacto.getPais().trim()
						.equals(""))) {
			query += "and ";
		}
*/
		if (contacto.getPais() != null && !contacto.getPais().trim().equals("")) {
			 cc.add(Restrictions.ilike("pais", contacto.pais.toLowerCase()));
			query += "Contacto.pais='" + contacto.getPais() + "' ";
		}
		
		listaContactos = Arrays.asList(orm.ContactoDAO.listContactoByCriteria(cc));
		
	//	listaContactos = orm.ContactoDAO.queryContacto(query, null);

		
		
		if (listaContactos != null) {
			for (orm.Contacto contactoOrm : listaContactos) {
				Contacto contactoNegocio = new Contacto();
				contactoNegocio.setUid(contactoOrm.getUid());
				contactoNegocio.setRun(contactoOrm.getRun());
				contactoNegocio.setNombre(contactoOrm.getNombre());
				contactoNegocio.setApellido(contactoOrm.getApellido());
				contactoNegocio.setTelefono(contactoOrm.getTelefono());
				contactoNegocio.setMail(contactoOrm.getMail());
				contactoNegocio.setCiudad(contactoOrm.getCiudad());
				contactoNegocio.setRegion(contactoOrm.getRegion());
				contactoNegocio.setPais(contactoOrm.getPais());
				contactoNegocio.setFoto(contactoOrm.getFoto());
				
				
				Empresa empresaNegocio = new Empresa();
				orm.Empresa empresaOrm = orm.EmpresaDAO.loadEmpresaByORMID(contactoOrm.getEmpresaU().getUid());
				empresaNegocio.setUid(empresaOrm.getUid());
				empresaNegocio.setRut(empresaOrm.getRut());
				empresaNegocio.setNombre(empresaOrm.getNombre());
				empresaNegocio.setTelefono(empresaOrm.getTelefono());
				empresaNegocio.setMail(empresaOrm.getMail());
				empresaNegocio.setCiudad(empresaOrm.getCiudad());
				empresaNegocio.setRegion(empresaOrm.getRegion());
				empresaNegocio.setPais(empresaOrm.getNombre());
				empresaNegocio.setRazonSocial(empresaOrm.getRazonSocial());
				empresaNegocio.setRepresentanteLegal(empresaOrm.getRepresentanteLegal());
				empresaNegocio.setRubro(empresaOrm.getRubro());

				contactoNegocio.setEmpresa(empresaNegocio);
				listaContacto.add(contactoNegocio);
			}
		}
		return listaContacto;
	}

	public List<Contacto> busquedaAvanzada2(Contacto contacto)
			throws PersistentException {
		List<Contacto> listaContacto = new ArrayList<Contacto>();
		List<orm.Contacto> listaContactos = new ArrayList<orm.Contacto>();
		String query = "";

		if (contacto.getRun() != null && !contacto.getRun().trim().equals("")) {
			query += "Contacto.run='" + contacto.getRun() + "' ";
		}

		if ((contacto.getRun() != null && !contacto.getRun().trim().equals(""))
				&& (contacto.getNombre() != null && !contacto.getNombre()
						.trim().equals(""))) {
			query += "and ";
		}
		if (contacto.getNombre() != null
				&& !contacto.getNombre().trim().equals("")) {
			query += "Contacto.nombre='" + contacto.getNombre() + "' ";
		}

		if (((contacto.getRun() != null && !contacto.getRun().trim().equals("")) || (contacto
				.getNombre() != null && !contacto.getNombre().trim().equals("")))
				&& (contacto.getApellido() != null && !contacto.getApellido()
						.trim().equals(""))) {
			query += "and ";
		}

		if (contacto.getApellido() != null
				&& !contacto.getApellido().trim().equals("")) {
			query += "Contacto.apellido='" + contacto.getApellido() + "' ";
		}

		if ((((contacto.getRun() != null && !contacto.getRun().trim()
				.equals("")) || (contacto.getNombre() != null && !contacto
				.getNombre().trim().equals(""))) || (contacto.getApellido() != null && !contacto
				.getApellido().trim().equals("")))
				&& (contacto.getMail() != null && !contacto.getMail().trim()
						.equals(""))) {
			query += "and ";
		}

		if (contacto.getMail() != null && !contacto.getMail().trim().equals("")) {
			query += "Contacto.mail='" + contacto.getMail() + "' ";
		}

		if (((((contacto.getRun() != null && !contacto.getRun().trim()
				.equals("")) || (contacto.getNombre() != null && !contacto
				.getNombre().trim().equals(""))) || (contacto.getApellido() != null && !contacto
				.getApellido().trim().equals(""))) || (contacto.getMail() != null && !contacto
				.getMail().trim().equals("")))
				&& (contacto.getTelefono() != null && !contacto.getTelefono()
						.trim().equals(""))) {
			query += "and ";
		}
		if (contacto.getTelefono() != null
				&& !contacto.getTelefono().trim().equals("")) {
			query += "Contacto.telefono='" + contacto.getTelefono() + "' ";
		}

		if ((((((contacto.getRun() != null && !contacto.getRun().trim()
				.equals("")) || (contacto.getNombre() != null && !contacto
				.getNombre().trim().equals(""))) || (contacto.getApellido() != null && !contacto
				.getApellido().trim().equals(""))) || (contacto.getMail() != null && !contacto
				.getMail().trim().equals(""))) || (contacto.getTelefono() != null && !contacto
				.getTelefono().trim().equals("")))
				&& (contacto.getRegion() != null && !contacto.getRegion()
						.trim().equals(""))) {
			query += "and ";
		}
		if (contacto.getRegion() != null
				&& !contacto.getRegion().trim().equals("")) {
			query += "Contacto.region='" + contacto.getRegion() + "' ";
		}

		if (((((((contacto.getRun() != null && !contacto.getRun().trim()
				.equals("")) || (contacto.getNombre() != null && !contacto
				.getNombre().trim().equals(""))) || (contacto.getApellido() != null && !contacto
				.getApellido().trim().equals(""))) || (contacto.getMail() != null && !contacto
				.getMail().trim().equals(""))) || (contacto.getTelefono() != null && !contacto
				.getTelefono().trim().equals(""))) || (contacto.getRegion() != null && !contacto
				.getRegion().trim().equals("")))
				&& (contacto.getCiudad() != null && !contacto.getCiudad()
						.trim().equals(""))) {
			query += "and ";
		}
		if (contacto.getCiudad() != null
				&& !contacto.getCiudad().trim().equals("")) {
			query += "Contacto.ciudad='" + contacto.getCiudad() + "' ";
		}

		if ((((((((contacto.getRun() != null && !contacto.getRun().trim()
				.equals("")) || (contacto.getNombre() != null && !contacto
				.getNombre().trim().equals(""))) || (contacto.getApellido() != null && !contacto
				.getApellido().trim().equals(""))) || (contacto.getMail() != null && !contacto
				.getMail().trim().equals(""))) || (contacto.getTelefono() != null && !contacto
				.getTelefono().trim().equals(""))) || (contacto.getRegion() != null && !contacto
				.getRegion().trim().equals(""))) || (contacto.getCiudad() != null && !contacto
				.getCiudad().trim().equals("")))
				&& (contacto.getPais() != null && !contacto.getPais().trim()
						.equals(""))) {
			query += "and ";
		}

		if (contacto.getPais() != null && !contacto.getPais().trim().equals("")) {
			query += "Contacto.pais='" + contacto.getPais() + "' ";
		}

		listaContactos = orm.ContactoDAO.queryContacto(query, null);

		if (listaContactos != null) {
			for (orm.Contacto contactoOrm : listaContactos) {
				Contacto contactoNegocio = new Contacto();
				contactoNegocio.setUid(contactoOrm.getUid());
				contactoNegocio.setRun(contactoOrm.getRun());
				contactoNegocio.setNombre(contactoOrm.getNombre());
				contactoNegocio.setApellido(contactoOrm.getApellido());
				contactoNegocio.setTelefono(contactoOrm.getTelefono());
				contactoNegocio.setMail(contactoOrm.getMail());
				contactoNegocio.setCiudad(contactoOrm.getCiudad());
				contactoNegocio.setRegion(contactoOrm.getRegion());
				contactoNegocio.setPais(contactoOrm.getPais());
				
				Empresa empresaNegocio = new Empresa();
				orm.Empresa empresaOrm = orm.EmpresaDAO.loadEmpresaByORMID(contactoOrm.getEmpresaU().getUid());
				empresaNegocio.setUid(empresaOrm.getUid());
				empresaNegocio.setRut(empresaOrm.getRut());
				empresaNegocio.setNombre(empresaOrm.getNombre());
				empresaNegocio.setTelefono(empresaOrm.getTelefono());
				empresaNegocio.setMail(empresaOrm.getMail());
				empresaNegocio.setCiudad(empresaOrm.getCiudad());
				empresaNegocio.setRegion(empresaOrm.getRegion());
				empresaNegocio.setPais(empresaOrm.getNombre());
				empresaNegocio.setRazonSocial(empresaOrm.getRazonSocial());
				empresaNegocio.setRepresentanteLegal(empresaOrm.getRepresentanteLegal());
				empresaNegocio.setRubro(empresaOrm.getRubro());

				contactoNegocio.setEmpresa(empresaNegocio);
				listaContacto.add(contactoNegocio);
			}
		}
		return listaContacto;
	}
	
}
