<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<jsp:include page="FormularioMenu.jsp" />
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link
	href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css"
	rel="stylesheet"></link>
<link
	href="//oss.maxcdn.com/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css"
	rel="stylesheet"></link>

<script src="//oss.maxcdn.com/jquery/1.11.1/jquery.min.js"></script>
<script
	src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script
	src="//oss.maxcdn.com/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.min.js"></script>

<script type="text/javascript" src="js/validador.js"></script>
<script type="text/javascript" src="js/image.js"></script>
<title>Ingreso Contacto Bitacora</title>
</head>
<%
	String uid = request.getParameter("uid");
%>

<body>
	<form action="ServletBitacora" id="ServletBitacora" method="post"
		class="form-horizontal mitad" action="#">

		<div class="page-header">
			<h1>
				Ingreso de Contacto Bitacora<small>Rellenar datos</small>
			</h1>
		</div>

		<div class="form-group">
			<label class="col-lg-3 control-label">Titulo</label>
			<div class="col-lg-3">
				<input type="text" class="form-control" name="titulo" required
					placeholder="Ingrese Titulo"> <br>
			</div>
		</div>

		<div class="form-group">
			<label class="col-lg-3 control-label">Comentario</label>
			<div class="col-lg-3">
				<textarea name="comentario" cols="40" rows="5">Comentario</textarea>

				<br>
			</div>
		</div>


		<div class="form-group">
			<label class="col-lg-3 control-label"></label>
			<div class="col-lg-3">
				<input style="visibility: hidden" type="text" class="form-control"
					name="uid" value="<%=uid%>" required> <br>
			</div>
		</div>


		<div class="col-lg-9 col-lg-offset-3">
			<button type="submit" class="btn btn-success left">Enviar</button>
		</div>
		</div>


	</form>


</body>

</html>