function encodeImage(){
    
    var imagenSubida = document.getElementById("inputImagen").files;
    //Get the element with the specified ID:
    if (imagenSubida.length > 0)
    {
        var fileToLoad = imagenSubida[0];
        var fileReader = new FileReader();

        fileReader.onload = function(fileLoadedEvent) {
            var srcData = fileLoadedEvent.target.result; 
            var newImage = document.createElement('img'); 
            newImage.src = srcData;
            document.getElementById("imgContainer").innerHTML = newImage.outerHTML;
            document.getElementById("foto").innerHTML = newImage.src;
        }
        fileReader.readAsDataURL(fileToLoad);
    }
}