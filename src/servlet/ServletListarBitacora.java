package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.orm.PersistentException;

import capanegocio.Bitacora;
import capanegocio.Contacto;

/**
 * Servlet implementation class ServletListarBitacora
 */
public class ServletListarBitacora extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletListarBitacora() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		List<Bitacora> lista = new ArrayList<>();
		// response.getWriter().append("Served at: ").append(request.getContextPath());
		try {
			lista = Bitacora.listar();
			
			PrintWriter out = response.getWriter();
			out.println("Listando Contactos");
			out.println("");
		
			for (Bitacora bitacora : lista) {
				out.println("titulo: " + bitacora.getTitulo());
				out.println("comentario: " + bitacora.getComentario());
			
			
				//out.println("Foto:" + contacto.getFoto());
				out.println("contacto run: " + bitacora.getContacto().getRun());
				out.println("contacto nombre: " + bitacora.getContacto().getNombre());
				out.println("contacto apellido: " + bitacora.getContacto().getApellido());
				//out.println("contacto empresa: " + bitacora.getContacto().getEmpresa().getNombre());
				
			}
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		request.setAttribute("bitacora", lista);
		request.getRequestDispatcher("/FormularioListarBitacora.jsp").forward(
				request, response);

	}

	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	
	}

}
