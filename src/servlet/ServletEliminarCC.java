package servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.orm.PersistentException;

import capanegocio.Bitacora;
import capanegocio.Contacto;

/**
 * Servlet implementation class ServletEliminarCC
 */
public class ServletEliminarCC extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ServletEliminarCC() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {


		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	/**
	 * doPost permite la eliminacion de un contacto
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out = response.getWriter();
		//String uid5= request.getParameter("id");  
	
		int id = Integer.parseInt(request.getParameter("id"));
		String r="";
		String mensaje="";
		String m = "";
		ServletEliminarCC borrarr = new ServletEliminarCC();
		borrarr.validacionId(id);
		Contacto eliminar = new Contacto();
		//String	contacto= request.getParameter("id");
	
	
		if (id < 0) {
			System.out.println("variable vacia");
		} else {
			eliminar.setUid(id);
			m = " Usuario con la id: " + id + " Eliminado";
			out.println("Contacto con la id:" + id + " Eliminado");
			
			try {
				Contacto.borrar(eliminar);
				
				mensaje= "Ingreso Completado";
				RequestDispatcher rs = request.getRequestDispatcher("FormularioMenu.jsp");
				request.setAttribute("mensaje", mensaje);
				rs.forward(request, response);
			} catch (PersistentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		
	}}

	/**
	 * M�todo que permite validar si id es entero
	 * @param id
	 * @return boolean
	 */
	public boolean validacionId(int id) {
		String cad = String.valueOf(id);
		for (int i = 0; i < cad.length(); i++)
			if (!Character.isDigit(cad.charAt(i))) {
				return false;
			}
		return true;
	}



	/**
	 * @see HttpServlet#doPut(HttpServletRequest, HttpServletResponse)
	 */
	protected void doPut(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doDelete(HttpServletRequest, HttpServletResponse)
	 */
	protected void doDelete(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
