package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.orm.PersistentException;

import capanegocio.Usuario;

/**
 * Servlet implementation class Login
 */
public class ServletLogin extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletLogin() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		

		HttpSession session = request.getSession();
		session.invalidate();
		RequestDispatcher rs = request.getRequestDispatcher("formularioLogin.jsp");
		request.setAttribute("LoginStatus",	"Se ha cerrado la session correctamente");
		rs.forward(request, response);
	  
        
	}
       
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	/**
	 * doPost permite el login de un usuario
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		String user = request.getParameter("usuario");
		String pass = request.getParameter("password");
		
		Usuario usuarioAV = new Usuario();
		usuarioAV.setUsuario(user);
		usuarioAV.setPassword(pass);
		
		//String LoginStatus = "";
		
		try {
			if(usuarioAV.validarU(usuarioAV)){
				RequestDispatcher rs = request.getRequestDispatcher("FormularioMenu.jsp");
				session.setAttribute("user", usuarioAV);
				rs.forward(request, response);
			}else{				
				RequestDispatcher rs = request.getRequestDispatcher("formularioLogin.jsp");
				request.setAttribute("LoginStatus",	"Error en los datos ingresados");
				rs.forward(request, response);
			}
		} catch (PersistentException e) {
			RequestDispatcher rs = request.getRequestDispatcher("formularioLogin.jsp");
			request.setAttribute("LoginStatus",	e.toString() );
		}
	}
	    }
	

