<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="i"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link
	href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css"
	rel="stylesheet"></link>
<link
	href="//oss.maxcdn.com/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css"
	rel="stylesheet"></link>

<script src="//oss.maxcdn.com/jquery/1.11.1/jquery.min.js"></script>
<script
	src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script
	src="//oss.maxcdn.com/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.min.js"></script>

<jsp:include page="FormularioMenu.jsp" />

<title>listar Contacto</title>
<title>Lista Contactos Empresariales</title>
</head>

<body>
	<form action="ServletListarCC" method="get">
	<div class="page-header">
		<h1>
			Lista de Contactos
		</h1>
	</div>
		<% try{ 
	request.getAttribute("contacto").toString(); %>
<table class="table table-bordered table-hover table-responsive">
		<tr class="success">
		<thead>
		
			<th>Run</th>
			<th>Nombre</th>
			<th>Apellidos</th>
			<th>Mail</th>
			<th>Teléfono</th>
			<th>País</th>
			<th>Región</th>
			<th>Ciudad</th>
			<th>Foto</th>
			<th>Empresa</th>
			

		</thead>
		</tr>
		<tbody>
			<i:forEach items="${contacto}" var="contacto">
				<tr>
				
					<td>${contacto.run}</td>
					<td>${contacto.nombre}</td>
					<td>${contacto.apellido}</td>
					<td>${contacto.mail}</td>
					<td>${contacto.telefono}</td>
					<td>${contacto.pais}</td>
					<td>${contacto.region}</td>
					<td>${contacto.ciudad}</td>
					<td><img src="${contacto.foto}" class="img-responsive"></img></td>
					
					<td>${contacto.empresa.nombre}</td>
				
					<td>
			
						<table>
							<tr>
								<td>
								<form action="FormularioQueAyuda.jsp" method="post">
								<input type="hidden" value="Eliminar" class="btn btn-danger">	
								</form>
								</td>
								
								<form action="FormularioBitacora.jsp" method="post">
									<input type="hidden" value="${contacto.uid}" name="uid">
									<input type="submit" value="Ingresar Bitacora" class="btn btn-info">	
								</form>
								</td>
								
								<td><form action="traspaso" method="post">
									<input type="hidden" value="${contacto.uid}" name="id">
									<input type="hidden" value="${contacto.run}" name="run">
								    <input type="hidden" value="${contacto.nombre}" name="nombre">
								     <input type="hidden" value="${contacto.apellido}" name="apellido">
								      <input type="hidden" value="${contacto.mail}" name="mail">
								       <input type="hidden" value="${contacto.telefono}" name="telefono">
								        <input type="hidden" value="${contacto.ciudad}" name="ciudad">
								         <input type="hidden" value="${contacto.region}" name="region">
								          <input type="hidden" value="${contacto.pais}" name="pais">
								         <input type="hidden" value="${contacto.foto}" name="foto">
								          
									<input type="submit" value="Editar" class="btn btn-primary">	
								</form>
								</td>
								<td>
								
								<form action="FormularioEliminarContacto.jsp" method="post">
									<input type="hidden" value="${contacto.uid}" name="id">
									<input type="submit" value="Eliminar" class="btn btn-danger">	
								</form>
								</td>
								
								<td>
								
								
								
								
							</tr>
						</table>
					</td>



				</tr>
			</i:forEach>
		</tbody>
	</table>
<%
}catch(NullPointerException e){
}
%>

		<button type="submit" class="btn btn-info">Listar
			Contactos</button>
	</form>

</body>
</html>