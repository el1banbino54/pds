package capanegocio;

import java.util.ArrayList;
import java.util.List;

import org.orm.PersistentException;
import org.orm.PersistentTransaction;

public class Bitacora {

	private String comentario;
	private String titulo;
	private int uid;
	private Contacto contacto;
	
	
	/**
	 * 
	 * @return int Uid de bitacora
	 */
	public int getUid() {
		return uid;
	}
/**
 * 
 * @param uid de bitacora
 */
	public void setUid(int uid) {
		this.uid = uid;
	}
/**
 * 
 * @return String Comentario de bitacora
 */
	public String getComentario() {
		return comentario;
	}
/**
 * 
 * @param comentario de bitacora
 */
	public void setComentario(String comentario) {
		this.comentario = comentario;
	}
	
	/**
	 * 
	 * @return Contacto de contacto en bitacora
	 */

	public Contacto getContacto() {
		return contacto;
	}
/**
 * 
 * @param contacto de bitacora
 */
	public void setContacto(Contacto contacto) {
		this.contacto = contacto;
	}
/**
 * 
 * @return String  tirulo en bitacora
 */
	public String getTitulo() {
		return titulo;
	}
/**
 * 
 * @param titulo de bitacora
 */
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	
	/**
	 * M�todo ingresar permite registrar una bitacora
	 * 
	 * @param bitacora
	 * @return retorna un String que en este caso seria la variable mensaje
	 * @throws PersistentException
	 */
	public static String ingresar(Bitacora bitacora) throws PersistentException {
		PersistentTransaction t = orm.Taller1MagisterInformaticaPersistentManager
				.instance().getSession().beginTransaction();
		String mensaje = "bitacora no ingresada";
		try {
			orm.Bitacora lormBitacora = orm.BitacoraDAO.createBitacora();
			
			orm.Contacto lormContacto = orm.ContactoDAO.loadContactoByORMID(bitacora.getContacto().getUid());

			lormBitacora.setComentario(bitacora.getComentario());
			lormBitacora.setTitulo(bitacora.getTitulo());
		//	lormContacto.setRun(lormContacto.getRun());
			lormBitacora.setContactou(lormContacto);
		
		
			
			orm.BitacoraDAO.save(lormBitacora);
			t.commit();
			mensaje = "bitacora ingresada";
		} catch (Exception e) {
			t.rollback();
		}
		return mensaje;

	}
	
	
	/**
	 * M�todo listar() que lista los contactos con su bitacoraalmacenados en la BD 
	 * 
	 * @return retorna una lista de contactos con su bitacora
	 * @throws PersistentException
	 */
	public static List<Bitacora> listar() throws PersistentException {

		List<orm.Bitacora> listaBitacoraOrm = orm.BitacoraDAO.queryBitacora(
				null, null);
		List<Bitacora> listaBitacora = new ArrayList<>();

		for (orm.Bitacora bitacoraOrm : listaBitacoraOrm) {
			Bitacora bitacoraNegocio = new Bitacora();
			Contacto contactoNegocio = new Contacto();
			orm.Contacto contactoOrm = orm.ContactoDAO
					.loadContactoByORMID(bitacoraOrm.getContactou().getUid());

			contactoNegocio.setUid(contactoOrm.getUid());
			contactoNegocio.setRun(contactoOrm.getRun());
			contactoNegocio.setNombre(contactoOrm.getNombre());
			contactoNegocio.setApellido(contactoOrm.getApellido());
			contactoNegocio.setFoto(contactoOrm.getFoto());

			bitacoraNegocio.setContacto(contactoNegocio);

			bitacoraNegocio.setUid(bitacoraOrm.getUid());
			bitacoraNegocio.setTitulo(bitacoraOrm.getTitulo());
			bitacoraNegocio.setComentario(bitacoraOrm.getComentario());
			listaBitacora.add(bitacoraNegocio);
			
		}

		return listaBitacora;
	}
	public static String borrar(Bitacora bitacora) throws PersistentException {
		PersistentTransaction t = orm.Taller1MagisterInformaticaPersistentManager
		.instance().getSession().beginTransaction();
	
		String mensaje = "no borrado";
		try {
			orm.Bitacora lormBitacora = orm.BitacoraDAO
					.loadBitacoraByORMID(bitacora.getUid());
			
			// Delete the persistent object
			orm.BitacoraDAO.delete(lormBitacora);
			t.commit();
			mensaje = "borrado";
		} catch (Exception e) {
			t.rollback();
		}
		return mensaje;
	}
	
	public static String verificar(Bitacora bitacora) throws PersistentException {
		PersistentTransaction t = orm.Taller1MagisterInformaticaPersistentManager
		.instance().getSession().beginTransaction();
		String mensaje = " borrado ";
		Contacto c=new Contacto();
		try {
			orm.Bitacora lormBitacora = orm.BitacoraDAO
					.loadBitacoraByORMID(bitacora.getUid());
			
		if(c.getUid()==bitacora.contacto.getUid()){
			mensaje = "no borrado aun ahi datos";
		}
			
			t.commit();
			
		} catch (Exception e) {
			t.rollback();
		}
		return mensaje;
	}
}
