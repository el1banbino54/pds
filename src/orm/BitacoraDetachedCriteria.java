/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class BitacoraDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression uid;
	public final StringExpression titulo;
	public final StringExpression comentario;
	public final IntegerExpression contactouId;
	public final AssociationExpression contactou;
	
	public BitacoraDetachedCriteria() {
		super(orm.Bitacora.class, orm.BitacoraCriteria.class);
		uid = new IntegerExpression("uid", this.getDetachedCriteria());
		titulo = new StringExpression("titulo", this.getDetachedCriteria());
		comentario = new StringExpression("comentario", this.getDetachedCriteria());
		contactouId = new IntegerExpression("contactou.uid", this.getDetachedCriteria());
		contactou = new AssociationExpression("contactou", this.getDetachedCriteria());
	}
	
	public BitacoraDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, orm.BitacoraCriteria.class);
		uid = new IntegerExpression("uid", this.getDetachedCriteria());
		titulo = new StringExpression("titulo", this.getDetachedCriteria());
		comentario = new StringExpression("comentario", this.getDetachedCriteria());
		contactouId = new IntegerExpression("contactou.uid", this.getDetachedCriteria());
		contactou = new AssociationExpression("contactou", this.getDetachedCriteria());
	}
	
	public ContactoDetachedCriteria createContactouCriteria() {
		return new ContactoDetachedCriteria(createCriteria("contactou"));
	}
	
	public Bitacora uniqueBitacora(PersistentSession session) {
		return (Bitacora) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public Bitacora[] listBitacora(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (Bitacora[]) list.toArray(new Bitacora[list.size()]);
	}
}

