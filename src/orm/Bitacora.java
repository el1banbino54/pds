/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class Bitacora {
	public Bitacora() {
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == orm.ORMConstants.KEY_BITACORA_CONTACTOU) {
			this.contactou = (orm.Contacto) owner;
		}
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	private int uid;
	
	private String titulo;
	
	private String comentario;
	
	private orm.Contacto contactou;
	
	public void setComentario(String value) {
		this.comentario = value;
	}
	
	public String getComentario() {
		return comentario;
	}
	
	private void setUid(int value) {
		this.uid = value;
	}
	
	public int getUid() {
		return uid;
	}
	
	public int getORMID() {
		return getUid();
	}
	
	public void setTitulo(String value) {
		this.titulo = value;
	}
	
	public String getTitulo() {
		return titulo;
	}
	
	public void setContactou(orm.Contacto value) {
		if (contactou != null) {
			contactou.bitacora.remove(this);
		}
		if (value != null) {
			value.bitacora.add(this);
		}
	}
	
	public orm.Contacto getContactou() {
		return contactou;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Contactou(orm.Contacto value) {
		this.contactou = value;
	}
	
	private orm.Contacto getORM_Contactou() {
		return contactou;
	}
	
	public String toString() {
		return String.valueOf(getUid());
	}
	
}
