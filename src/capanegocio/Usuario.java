package capanegocio;

import java.util.ArrayList;
import java.util.List;

import org.orm.PersistentException;
import org.orm.PersistentTransaction;

import servlet.ServletAgregarU;

public class Usuario {

	public Usuario() {

	}

	private static final int ROW_COUNT = 100;

	private int uid;

	private String usuario;

	private String password;

	public int getUid() {
		return uid;
	}

	public void setUid(int uid) {
		this.uid = uid;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * M�todo ingresar permite crear un usuario en la BD
	 * 
	 * @param usuario
	 * @return retorna un String en con su variable msg
	 * @throws PersistentException
	 */
	public static String ingresar(Usuario usuario) throws PersistentException {
		PersistentTransaction t = orm.Taller1MagisterInformaticaPersistentManager
				.instance().getSession().beginTransaction();
		String msg = "ingreso no exitoso de usuario";
		try {
			orm.Usuario lormUsuario = orm.UsuarioDAO.createUsuario();
			// ServletAgregarU servlet = new ServletAgregarU();
			// Initialize the properties of the persistent object here
			try {
				lormUsuario.setUsuario(usuario.getUsuario());
			} catch (NullPointerException e) {
				e.printStackTrace();
			}
			try {
				lormUsuario.setPassword(usuario.getPassword());
			} catch (NullPointerException e) {
				e.printStackTrace();
			}

			msg = "Ingreso Exitoso de usuario";
			orm.UsuarioDAO.save(lormUsuario);
			t.commit();
		} catch (Exception e) {
			t.rollback();
		}
		return msg;

	}

	/**
	 * M�todo actualizar permite modificar los datos de un usuario en la BD
	 * 
	 * @param usuario
	 * @return retorna un String con su variable msg
	 * @throws PersistentException
	 */
	public static String actualizar(Usuario usuario) throws PersistentException {
		PersistentTransaction t = orm.Taller1MagisterInformaticaPersistentManager
				.instance().getSession().beginTransaction();
		String msg = "actualizacion no exitosa";
		try {
			orm.Usuario lormUsuario = orm.UsuarioDAO
					.loadUsuarioByORMID(usuario.uid);
			// Update the properties of the persistent object

			try {
				lormUsuario.setUsuario(usuario.getUsuario());
			} catch (NullPointerException e) {
				e.printStackTrace();
			}
			try {
				lormUsuario.setPassword(usuario.getPassword());
			} catch (NullPointerException e) {
				e.printStackTrace();
			}

			msg = "Actualizacion exitosa de usuario";
			orm.UsuarioDAO.save(lormUsuario);
			t.commit();

		} catch (Exception e) {
			t.rollback();
		}
		return msg;

	}

	/**
	 * M�todo borrar permite eliminar un usuario de la BD
	 * 
	 * @param usuario
	 * @return retorna un String con su variable msg
	 * @throws PersistentException
	 */
	public static String borrar(Usuario usuario) throws PersistentException {
		PersistentTransaction t = orm.Taller1MagisterInformaticaPersistentManager
				.instance().getSession().beginTransaction();
		String msg = "no borrado";
		try {
			orm.Usuario lormUsuario = orm.UsuarioDAO
					.loadUsuarioByORMID(usuario.uid);
			// Delete the persistent object

			orm.UsuarioDAO.delete(lormUsuario);
			t.commit();
			msg = "borrado";
		} catch (Exception e) {
			t.rollback();
		}
		return msg;
	}

	/**
	 * M�todo listar permite ver los usuarios que estan en la BD
	 * 
	 * @param usuario
	 * @return retorna en una lista los usuario
	 * @throws PersistentException
	 */
	public static List<Usuario> listar() throws PersistentException {

		List<orm.Usuario> listaUsuarioOrm = orm.UsuarioDAO.queryUsuario(null,
				null);
		List<Usuario> listaUsuario = new ArrayList<>();

		for (orm.Usuario usuarioOrm : listaUsuarioOrm) {
			Usuario usuario = new Usuario();
			usuario.setUsuario(usuarioOrm.getUsuario());
			usuario.setPassword(usuarioOrm.getPassword());

			listaUsuario.add(usuario);
			System.out.println("Listando");
		}

		return listaUsuario;
	}

	/**
	 * M�todo buscar permite la busqueda de un usuario en la BD
	 * 
	 * @param usuario
	 * @return
	 * @throws PersistentException
	 */

	public static Usuario busquedaUsuario(Usuario usuarioRec)
			throws PersistentException {
		Usuario usuario = new Usuario();
		try {
			orm.Usuario usuarioOrm = orm.UsuarioDAO.loadUsuarioByQuery(
					"Usuario.usuario='" + usuarioRec.getUsuario()
							+ "'AND Usuario.password='" + usuarioRec.getPassword()
							+ "'", null);

			usuario.setUsuario(usuarioOrm.getUsuario());
			usuario.setPassword(usuarioOrm.getPassword());

			System.out.println(" record(s) retrieved.");
			return usuario;
		} catch (NullPointerException e) {
			e.printStackTrace();

			return usuario;
		}
	}
	/**
	 * Permite validar que el usuario este logeado
	 * @param usuario
	 * @return 
	 * @throws PersistentException
	 */
	public boolean validarU(Usuario usuario) throws PersistentException{
		orm.Taller1MagisterInformaticaPersistentManager.instance().getSession().beginTransaction();
		boolean validador = false;
		
		orm.Usuario[] u; 
		u = orm.UsuarioDAO.listUsuarioByQuery("Usuario.usuario = '"+usuario.getUsuario()+"' AND Usuario.password = '"+usuario.getPassword()+"'", null);
		//if(usuario.getUsuario()=="Usuario.usuario"&& usuario.getPassword()=="Usuario.password"){
			//validador = true;
			//return validador;
		//}else{
			//return validador;
		//}
		
		if(u.length > 0 ){
			validador = true;
			return validador;
		}else{
			return validador;
		}
	}
}
