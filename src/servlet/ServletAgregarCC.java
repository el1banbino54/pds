package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.orm.PersistentException;
import org.orm.PersistentTransaction;

import capanegocio.Contacto;
import capanegocio.Empresa;
import ormsamples.CreateTaller1MagisterInformaticaData;

/**
 * Servlet implementation class TallerServlet
 */

public class ServletAgregarCC extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String PATTERN_EMAIL = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
	            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletAgregarCC() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	
		
		
		HttpSession session = request.getSession();
		session.invalidate();
		RequestDispatcher rs = request.getRequestDispatcher("formularioLogin.jsp");
		request.setAttribute("LoginStatus",	"Se ha cerrado la session correctamente");
		rs.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	/**
	 * Método que agrega Contactos
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out= response.getWriter();
		PersistentTransaction t = null;
		ServletAgregarCC agregar = new ServletAgregarCC();
		Empresa oEmpresa = new Empresa();
		Contacto oContacto = new Contacto(); 
		String mensaje="";
	
		String	run= request.getParameter("run");
		String	nombre= request.getParameter("nombre");
		String	apellido= request.getParameter("apellido");
		String	mail= request.getParameter("mail");
		String	telefono= request.getParameter("telefono");
		String	pais= request.getParameter("pais");
		String	region= request.getParameter("region");
		String	ciudad= request.getParameter("ciudad");	
		String 	foto=request.getParameter("foto");
		String	comentario= request.getParameter("comentario");
		String	empresa= request.getParameter("empresa");
		
		int empresaId = Integer.parseInt(empresa);
       
		agregar.validacionRun(run);			
		agregar.validacionMail(mail);
		agregar.validacionEntero(telefono);
			
		if ((agregar.validacionRun(run) == true) && (agregar.validacionMail(mail) == true)
					&& (agregar.validacionEntero(telefono))){
			
		
		if(run.trim().equals("") || nombre.trim().equals("") || apellido.trim().equals("")||
						mail.trim().equals("") || telefono.trim().equals("") || pais.trim().equals("") || 
						region.trim().equals("") || ciudad.trim().equals("") ||
						empresaId < 0){
					System.out.println("variable vacia");
					
		}else{
		if (run.length() <=12 && nombre.length() <=50 && apellido.length() <=50 && mail.length() <=20 && 
			telefono.length() <= 20 && pais.length() <= 20 && region.length() <= 20 && 
			ciudad.length() <= 20 ){

						
						try{
							oContacto.setRun(run);
						}catch (NullPointerException e) {
							e.printStackTrace();
						}
						try{
							oContacto.setNombre(nombre);
						}catch (NullPointerException e){
							e.printStackTrace();
						}
						try{
							oContacto.setApellido(apellido);
						}catch (NullPointerException e){
							e.printStackTrace();
						}
						try{
							oContacto.setMail(mail);
						}catch (NullPointerException e){
							e.printStackTrace();
						}
						try{
							oContacto.setTelefono(telefono);
						}catch (NullPointerException e){
							e.printStackTrace();
						}
						try{
							oContacto.setPais(pais);
						}catch (NullPointerException e){
							e.printStackTrace();
						}
						try{
							oContacto.setRegion(region);
						}catch (NullPointerException e){
							e.printStackTrace();
						}
						try{
							oContacto.setCiudad(ciudad);
						}catch (NullPointerException e){
							e.printStackTrace();
						}
						
						try{
						oContacto.setFoto(foto);
						}catch (NullPointerException e){
							e.printStackTrace();
						}
						try{
							oEmpresa.setUid(empresaId);
						}catch (NullPointerException e){
							e.printStackTrace();
						}
						
						
						try{
							oContacto.setEmpresa(oEmpresa);
						}catch (NullPointerException e){
							e.printStackTrace();
						}
						
						String r="";
						
						try {
							r=Contacto.ingresar(oContacto);
							mensaje= "Ingreso Completado";
							RequestDispatcher rs = request.getRequestDispatcher("FormularioMenu.jsp");
							request.setAttribute("mensaje", mensaje);
							rs.forward(request, response);
						} catch (PersistentException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
					}
				} else {
						mensaje = "No Ingresado";
						RequestDispatcher rs = request.getRequestDispatcher("FormularioIngresoContacto.jsp");
						request.setAttribute("mensaje", mensaje);
						rs.forward(request, response);
					}
					
			}
		} else { 
				mensaje = "No Ingresado";
				RequestDispatcher rs = request.getRequestDispatcher("FormularioIngresoContacto.jsp");
				request.setAttribute("mensaje", mensaje);
				rs.forward(request, response);
			}
	}

	/**
	 * Metodo que permite validar el mail
	 * @param email
	 * @return boolean
	 */
	public boolean validacionMail(String email) {
		 
        // Compiles the given regular expression into a pattern.
        Pattern pattern = Pattern.compile(PATTERN_EMAIL);
 
        // Match the given input against this pattern
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
 
    }
	/**
	 * Método que permite validar si id entero
	 * @param id
	 * @return boolean
	 */
	public boolean validacionEntero(String cad){
		 for(int i = 0; i<cad.length(); i++)
		 if( !Character.isDigit(cad.charAt(i)) ){
			 return false;
		 }
		 return true;
	}
	

	/**
	 * Método que permite validar el run
	 * @param run
	 * @return boolean
	 */
	public static boolean validacionRun(String run) {
		 
		boolean validacion = false;
		try {
			run = run.toUpperCase();
			run = run.replace(".", "");
			run = run.replace("-", "");
			int runAux = Integer.parseInt(run.substring(0, run.length() - 1));
			 
			char dv = run.charAt(run.length() - 1);
			 
			int m = 0, s = 1;
			for (; runAux != 0; runAux /= 10) {
				s = (s + runAux % 10 * (9 - m++ % 6)) % 11;
			}
			if (dv == (char) (s != 0 ? s + 47 : 75)) {
				validacion = true;
			}
			 
		} catch (java.lang.NumberFormatException e) {
		} catch (Exception e) {
		}
		return validacion;
	}
	
	/**
	 * @see HttpServlet#doPut(HttpServletRequest, HttpServletResponse)
	 */
	protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doDelete(HttpServletRequest, HttpServletResponse)
	 */
	protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}