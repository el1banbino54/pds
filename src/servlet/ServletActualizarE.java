package servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.orm.PersistentException;
import org.orm.PersistentTransaction;

import capanegocio.Empresa;

;

/**
 * Servlet implementation class ServletActualizarE
 */
public class ServletActualizarE extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ServletActualizarE() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	/**
	 * doPost permite Actualizar Empresa
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		PersistentTransaction t = null;
		PrintWriter out = response.getWriter();
		String m = "";
		try {
			int id = Integer.parseInt(request.getParameter("id"));
			String rut = request.getParameter("rut");
			String nombre = request.getParameter("nombre");
			String direccion = request.getParameter("direccion");
			String telefono = request.getParameter("telefono");
			String mail = request.getParameter("mail");
			String ciudad = request.getParameter("ciudad");
			String region = request.getParameter("region");
			String pais = request.getParameter("pais");
			String representanteLegal = request
					.getParameter("representanteLegal");
			String razonSocial = request.getParameter("razonSocial");
			String rubro = request.getParameter("rubro");

			ServletActualizarE refrescar = new ServletActualizarE();
			refrescar.validarId(id);

			Empresa actualizar = new Empresa();

			if (id < 0 || rut.trim().equals("") || id < 0
					|| nombre.trim().equals("") || id < 0
					|| direccion.trim().equals("") || id < 0
					|| telefono.trim().equals("") || id < 0
					|| mail.trim().equals("") || id < 0
					|| ciudad.trim().equals("") || id < 0
					|| region.trim().equals("") || id < 0
					|| pais.trim().equals("") || id < 0
					|| razonSocial.trim().equals("") || id < 0
					|| representanteLegal.trim().equals("") || id < 0
					|| rubro.trim().equals("")) {
				System.out.println("variable vacia");
			} else {
				if (rut.length() <= 20 && nombre.length() <= 20
						&& direccion.length() <= 20 && telefono.length() <= 20
						&& mail.length() <= 20 && ciudad.length() <= 20
						&& region.length() <= 20 && pais.length() <= 20
						&& razonSocial.length() <= 20
						&& representanteLegal.length() <= 20
						&& rubro.length() <= 20) {

					m = " Actualizacion en la iddd: " + id + " Realizada";
					out.println(" Actualizacion en la id: " + id + " Realizada");

					actualizar.setUid(id);
					actualizar.setRut(rut);
					actualizar.setNombre(nombre);
					actualizar.setDireccion(direccion);
					actualizar.setTelefono(telefono);
					actualizar.setMail(mail);
					actualizar.setCiudad(ciudad);
					actualizar.setRegion(region);
					actualizar.setPais(pais);
					actualizar.setRazonSocial(razonSocial);
					actualizar.setRepresentanteLegal(representanteLegal);
					actualizar.setRubro(rubro);

					try {
						Empresa.actualizar(actualizar);
					} catch (PersistentException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		} catch (NullPointerException e) {
			e.printStackTrace();
		}
	}

	/**
	 * M�todo que permite validar que el formato de la cadena ingresada sea
	 * num�rico
	 * 
	 * @param cad
	 *          
	 * @return
	 */
	private boolean esEntero(String cad) {
		for (int i = 0; i < cad.length(); i++)
			if (!Character.isDigit(cad.charAt(i))) {
				return false;
			}
		return true;
	}

	/**
	 * M�todo que permite validar si el campo ingresado en id es entero
	 * 
	 * @param id
	 *         
	 * @return
	 */
	private boolean validarId(int id) {
		String cad = String.valueOf(id);
		for (int i = 0; i < cad.length(); i++)
			if (!Character.isDigit(cad.charAt(i))) {
				return false;
			}
		return true;
	}
}
