package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.util.List;

import org.orm.PersistentException;

import capanegocio.Contacto;
import capanegocio.Empresa;

/**
 * Servlet implementation class ServletBusquedaSimple
 */
public class ServletBusquedaSimple extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ServletBusquedaSimple() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		session.invalidate();
		RequestDispatcher rs = request.getRequestDispatcher("formularioLogin.jsp");
		request.setAttribute("LoginStatus",	"Se ha cerrado la session correctamente");
		rs.forward(request, response);
		
		//RequestDispatcher dispatcher = request
			//	.getRequestDispatcher("BusquedaSimple.jsp");
		//dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	/**
	 * doPost permite la busqueda simple de un contacto
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		String textoBusqueda = request.getParameter("textoBusqueda");
		Contacto contacto = new Contacto();
		List<Contacto> lista = new ArrayList<Contacto>();
		try {
			lista = contacto.busquedaSimple(textoBusqueda);
			//System.out.println(lista.get(0).getNombre());
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		request.removeAttribute("busqueda");
		request.setAttribute("busqueda", lista);
		request.getRequestDispatcher("FormularioBusquedaSimple.jsp").forward(
				request, response);

	}

}
