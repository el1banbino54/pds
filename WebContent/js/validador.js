$(document).ready(function() {
	$('#ServletLogin').bootstrapValidator({
		 message: 'Este valor no es valido',
		 feedbackIcons: {
			 valid: 'glyphicon glyphicon-ok',
			 invalid: 'glyphicon glyphicon-remove',
			 validating: 'glyphicon glyphicon-refresh'
		 },
		 fields: {
			 usuario: {
				 validators: {
					 notEmpty: {
						 message: 'El nombre de usuario es requerido'
					 }
				 }
			 },
			 password: {
				 validators: {
					 notEmpty: {
						 message: 'La contraseña es requerida'
					 }
				 }
			 }
		 }
	});
	
	
	$('#ServletBusquedaSimple').bootstrapValidator({
		 message: 'Este valor no es valido',
		 feedbackIcons: {
			 valid: 'glyphicon glyphicon-ok',
			 invalid: 'glyphicon glyphicon-remove',
			 validating: 'glyphicon glyphicon-refresh'
		 },
		 fields: {
			 textoBusqueda: {
				 validators: {
					 notEmpty: {
						 message: 'El nombre de usuario es requerido'
					 }
				 }
			 },
			 
			 
		 }
	});
	
	$('#ServletEliminarCC').bootstrapValidator({
		 message: 'Este valor no es valido',
		 feedbackIcons: {
			 valid: 'glyphicon glyphicon-ok',
			 invalid: 'glyphicon glyphicon-remove',
			 validating: 'glyphicon glyphicon-refresh'
		 },
		 fields: {
			 id: {
				 validators: {
					 notEmpty: {
						 message: 'El nombre de usuario es requerido'
					 }
				 }
			 },
			 
			 
		 }
	});
	
	
	$('#ServletAgregarCC').bootstrapValidator({
    	 feedbackIcons: {
    		 valid: 'glyphicon glyphicon-ok',
    		 invalid: 'glyphicon glyphicon-remove',
    		 validating: 'glyphicon glyphicon-refresh'
    	 },
    	 fields: {
    		
    		 run: {
    			 validators: {
    				 notEmpty: {
    					 message: 'El nombre es requerido'
    				 },
                     stringLength: {
                        
                    	 max: 12,
                         message: 'Maximo 12 caracteres'
                     }
    			 }
    		 },
    		 nombre: {
    			 validators: {
    				 notEmpty: {
    					 message: 'El nombre es requerido'
    				 },
                     stringLength: {
                         max: 20,
                         message: 'El nombre debe contener maxímo 20 caracteres'
                     }
    			 }
    		 },
    		 apellido: {
    			 validators: {
    				 notEmpty: {
    					 message: 'El apellido es requerido'
    				 },
                     stringLength: {
                         max: 20,
                         message: 'El apellido debe contener maximo 20 caracteres'
                     }
    			 }
    		 },
    		 mail: {
                 validators: {
                     notEmpty: {
                         message: 'El correo es requerido y no puede ser vacio'
                     },
                     emailAddress: {
                         message: 'El correo electronico no es valido'
                     },
                     stringLength: {
                         max:30,
                         message: 'El mail debe contener maximo 30 caracteres'
                     }
                 }   
             },
             telefono: {
                 validators: {
                     notEmpty: {
                         message: 'El telefono es requerido'
                     },
                   
                     stringLength: {
                         max: 20,
                         message: 'El telefono debe contener maximo 20 caracteres'
                     },
                     regexp: {
                         regexp: /^[0-9]+$/,
                         message: 'El telefono solo puede contener números'
                     }
                 }
             },
             pais: {
                 validators: {
                     notEmpty: {
                         message: 'El país es requerido'
                     },
                     stringLength: {
                         max: 20,
                         message: 'El país debe contener maximo 20 caracteres'
                     }
                 }
             },
             region: {
                 validators: {
                     notEmpty: {
                         message: 'La región es requerida'
                     },
                     stringLength: {
                         max: 20,
                         message: 'La región debe contener maximo 20 caracteres'
                     }
                 }
             },
             ciudad: {
                 validators: {
                     notEmpty: {
                         message: 'La ciudad es requerido'
                     },
                     stringLength: {
                         max: 20,
                         message: 'La ciudad debe contener maximo 20 caracteres'
                     }
                 }
             },
    	 }
    });

    $('#ServletActualizarCC').bootstrapValidator({
         feedbackIcons: {
             valid: 'glyphicon glyphicon-ok',
             invalid: 'glyphicon glyphicon-remove',
             validating: 'glyphicon glyphicon-refresh'
         },
         fields: {
             id: {
                 validators: {
                     notEmpty: {
                         message: 'El id es requerido'
                     },
                     stringLength: {
                         max: 20,
                         message: 'El id debe contener maximo 20 caracteres'
                     },
                     regexp: {
                         regexp: /^[0-9]+$/,
                         message: 'El id solo puede contener números'
                     }
                 }
             },
        	 run: {
    			 validators: {
    				 notEmpty: {
    					 message: 'El nombre es requerido'
    				 },
                     stringLength: {
                         max: 12,
                         message: 'maximo 12 caracteres'
                     }
    			 }
    		 },
             nombre: {
                 validators: {
                     notEmpty: {
                         message: 'El nombre es requerido'
                     },
                     stringLength: {
                         max: 20,
                         message: 'El nombre debe contener maximo 20 caracteres'
                     }
                 }
             },
             apellido: {
                 validators: {
                     notEmpty: {
                         message: 'El apellido es requerido'
                     },
                     stringLength: {
                         max: 20,
                         message: 'El apellido debe contener maximo 20 caracteres'
                     }
                 }
             },
             mail: {
                 validators: {
                     notEmpty: {
                         message: 'El correo es requerido y no puede ser vacio'
                     },
                     emailAddress: {
                         message: 'El correo electronico no es valido'
                     },
                     stringLength: {
                         max: 20,
                         message: 'El mail debe contener maximo 20 caracteres'
                     }
                 }   
             },
             telefono: {
                 validators: {
                     notEmpty: {
                         message: 'El telefono es requerido'
                     },
                    
                     stringLength: {
                         max: 20,
                         message: 'El telefono debe contener maximo 20 caracteres'
                     },
                     regexp: {
                         regexp: /^[0-9]+$/,
                         message: 'El teléfono solo puede contener números'
                     }
                 }
             },
             pais: {
                 validators: {
                     notEmpty: {
                         message: 'El país es requerido'
                     },
                     stringLength: {
                         max: 20,
                         message: 'El país debe contener maximo 20 caracteres'
                     }
                 }
             },
             region: {
                 validators: {
                     notEmpty: {
                         message: 'La región es requerida'
                     },
                     stringLength: {
                         max: 20,
                         message: 'La región debe contener maximo 20 caracteres'
                     }
                 }
             },
             ciudad: {
                 validators: {
                     notEmpty: {
                         message: 'La ciudad es requerido'
                     },
                     stringLength: {
                         max: 20,
                         message: 'La ciudad debe contener maximo 20 caracteres'
                     }
                 }
             },
         }
    });
	$('#ServletEliminarCC').bootstrapValidator({
		 message: 'Este valor no es valido',
		 feedbackIcons: {
			 valid: 'glyphicon glyphicon-ok',
			 invalid: 'glyphicon glyphicon-remove',
			 validating: 'glyphicon glyphicon-refresh'
		 },
		 fields: {
			 id: {
				 validators: {
					 notEmpty: {
						 message: 'El nombre de usuario es requerido'
					 }
				 }
			 },
			     regexp: {
                  regexp: /^[0-9]+$/,
                    message: 'Id solo puede tener numeros números'
             }
			 
			 
		 }
	});
	
	
	  
   
});