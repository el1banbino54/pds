package servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.orm.PersistentException;

import servlet.ServletEliminarCC;
import capanegocio.Contacto;
import capanegocio.Usuario;

/**
 * Servlet implementation class ServletElimiU
 */
public class ServletElimiU extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletElimiU() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
int id= Integer.parseInt(request.getParameter("id"));
		String m="";
		ServletElimiU borrar = new ServletElimiU();
		PrintWriter out= response.getWriter();
		borrar.validarId(id);
		Usuario eliminar = new Usuario();
		
		if(id<0){
			System.out.println("variable vacia");
		} else {
			eliminar.setUid(id);
			m=" Usuario con la id: "+ id+ "Eliminado";
			out.println("Contacto con la id:"+id+" Eliminado");
			try {
				Usuario.borrar(eliminar);
				
			} catch (PersistentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				
			}
		}
		
	}

	
	public boolean validarId(int id){
		String cad = String.valueOf(id);
		 for(int i = 0; i<cad.length(); i++)
		 if( !Character.isDigit(cad.charAt(i)) ){
			 return false;
		 }
		 return true;
	 }
	}


