<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="i"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link
	href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css"
	rel="stylesheet"></link>
<link
	href="//oss.maxcdn.com/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css"
	rel="stylesheet"></link>

<script src="//oss.maxcdn.com/jquery/1.11.1/jquery.min.js"></script>
<script
	src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script
	src="//oss.maxcdn.com/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.min.js"></script>

>

<title>Lista de empresas</title>
</head>
<body>



	<form action="ServletListarE" method="get">
		<div class="row">
			<div class="col-sm-1" style="background-color: aqua;">uid</div>
			<div class="col-sm-1" style="background-color: aqua;">nombre</div>
			<div class="col-sm-1" style="background-color: aqua;">
				direccion</div>
			<div class="col-sm-1" style="background-color: aqua;">rut</div>
			<div class="col-sm-1" style="background-color: aqua;">telefono</div>
			<div class="col-sm-1" style="background-color: aqua;">mail</div>
			<div class="col-sm-1" style="background-color: aqua;">ciudad</div>
			<div class="col-sm-1" style="background-color: aqua;">region</div>
			<div class="col-sm-1" style="background-color: aqua">pais</div>
			<div class="col-sm-1" style="background-color: aqua;">razonsocial
			</div>
			<div class="col-sm-1" style="background-color: aqua;">representanteLegal</div>
			<div class="col-sm-1" style="background-color: aqua;">rubro</div>


		</div>
		<i:forEach items="${cont}" var="empresa">

			<br />

			<div class="row">
				<div class="col-sm-1" style="background-color: aqua;">
					${empresa.uid}</div>
				<div class="col-sm-1" style="background-color: aqua;">
					${empresa.nombre}</div>
				<div class="col-sm-1" style="background-color: aqua;">
					${empresa.direccion}</div>
				<div class="col-sm-1" style="background-color: aqua;">
					${empresa.rut}</div>
				<div class="col-sm-1" style="background-color: aqua;">
					${empresa.telefono}</div>
				<div class="col-sm-1" style="background-color: aqua;">
					${empresa.mail}</div>
				<div class="col-sm-1" style="background-color: aqua;">
					${empresa.ciudad}</div>
				<div class="col-sm-1" style="background-color: aqua;">
					${empresa.region}</div>
				<div class="col-sm-1" style="background-color: aqua;">
					${empresa.pais}</div>
				<div class="col-sm-1" style="">${empresa.razonSocial}</div>
				<div class="col-sm-1" style="background-color: aqua;">
					${empresa.representanteLegal}</div>
				<div class="col-sm-1" style="background-color: aqua;">
					${empresa.rubro}</div>
			</div>
		</i:forEach>


		<button type="submit" class="btn btn-success left">Listar</button>
	</form>
</body>
</html>