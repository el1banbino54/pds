package servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.orm.PersistentException;
import org.orm.PersistentTransaction;

import capanegocio.Bitacora;
import capanegocio.Contacto;
import capanegocio.Empresa;

/**
 * Servlet implementation class ServletBitacora
 */
public class ServletBitacora extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletBitacora() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		session.invalidate();
		RequestDispatcher rs = request.getRequestDispatcher("formularioLogin.jsp");
		request.setAttribute("LoginStatus",	"Se ha cerrado la session correctamente");
		rs.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	/**
	 * M�todo que agrega Bitacoras
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stubPrintWriter out= response.getWriter();
		PrintWriter out= response.getWriter();
		PersistentTransaction t = null;

		ServletBitacora agregar = new ServletBitacora();
		Bitacora oBitacora = new Bitacora();
		Contacto oContacto = new Contacto(); 
		String mensaje="";

		String	titulo= request.getParameter("titulo");
		String	comentario= request.getParameter("comentario");
		String	uid= request.getParameter("uid");
		//String	run= request.getParameter("run");
		int contactoId = Integer.parseInt(uid);
		agregar.validacionEntero(uid);
						
	
		if ((agregar.validacionEntero(uid) == true)){ 

			if(titulo.trim().equals("") || comentario.trim().equals("") || uid.trim().equals("")){
				
			}else{
					if (titulo.length() <=255 && comentario.length() <=255 && uid.length()<=255){
		
						try{
							oBitacora.setTitulo(titulo);
						}catch (NullPointerException e) {
							e.printStackTrace();
						}
	
						try{
							oBitacora.setComentario(comentario);
						}catch (NullPointerException e) {
							e.printStackTrace();
						}
						try{
							oContacto.setUid(contactoId);
						}catch (NullPointerException e) {
							e.printStackTrace();
						}
						try{
							  oBitacora.setContacto(oContacto);
						}catch (NullPointerException e) {
							e.printStackTrace();
						}
						
						String r="";

							try {
								r=Bitacora.ingresar(oBitacora);
								mensaje= "Ingreso Completado";
								RequestDispatcher rs = request.getRequestDispatcher("FormularioMenu.jsp");
								request.setAttribute("mensaje", mensaje);
								rs.forward(request, response);
							} catch (PersistentException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
				}
				 else {
					mensaje = "No Ingresado";
					RequestDispatcher rs = request.getRequestDispatcher("FormularioIngresoContacto.jsp");
					request.setAttribute("mensaje", mensaje);
					rs.forward(request, response);
				}
			}
		
			}else { 
			mensaje = "No Ingresado";
			RequestDispatcher rs = request.getRequestDispatcher("FormularioIngresoContacto.jsp");
			request.setAttribute("mensaje", mensaje);
			rs.forward(request, response);
		}
}

		
		
	public boolean validacionEntero(String cad){
		 for(int i = 0; i<cad.length(); i++)
		 if( !Character.isDigit(cad.charAt(i)) ){
			 return false;
		 }
		 return true;
	}
}
	

