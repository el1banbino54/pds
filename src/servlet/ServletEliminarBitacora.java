package servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.orm.PersistentException;

import capanegocio.Bitacora;
import capanegocio.Contacto;

/**
 * Servlet implementation class ServletEliminarBitacora
 */
public class ServletEliminarBitacora extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletEliminarBitacora() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		session.invalidate();
		RequestDispatcher rs = request.getRequestDispatcher("formularioLogin.jsp");
		request.setAttribute("LoginStatus",	"Se ha cerrado la session correctamente");
		rs.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//String	uid= request.getParameter("id");
		PrintWriter out = response.getWriter();
	//	String uid3= request.getParameter("id");
		int id = Integer.parseInt(request.getParameter("uid"));
		
		String r="";
		String mensaje="";
		String m = "";
		ServletEliminarBitacora borrar = new ServletEliminarBitacora();
		borrar.validacionId(id);
		Bitacora eliminar = new Bitacora();

		if (id < 0) {
			System.out.println("variable vacia");
		} else {
			eliminar.setUid(id);
			m = " Usuario con la bitacora id: " + id + " Eliminado";
			out.println("Contacto conbitacora la id:" + id + " Eliminado");
			
			try {
				Bitacora.borrar(eliminar);
				
				mensaje= "Borrado";
				RequestDispatcher rs = request.getRequestDispatcher("FormularioMenu.jsp");
				request.setAttribute("mensaje", mensaje);
				rs.forward(request, response);
			} catch (PersistentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	}

	/**
	 * M�todo que permite validar si id es entero
	 * @param id
	 * @return boolean
	 */
	public boolean validacionId(int id) {
		String cad = String.valueOf(id);
		for (int i = 0; i < cad.length(); i++)
			if (!Character.isDigit(cad.charAt(i))) {
				return false;
			}
		return true;
	}
	}


