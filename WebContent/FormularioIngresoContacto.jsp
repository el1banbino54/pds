<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<jsp:include page="FormularioMenu.jsp" />

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link
	href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css"
	rel="stylesheet"></link>
<link
	href="//oss.maxcdn.com/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css"
	rel="stylesheet"></link>

<script src="//oss.maxcdn.com/jquery/1.11.1/jquery.min.js"></script>
<script
	src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script
	src="//oss.maxcdn.com/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.min.js"></script>

<script type="text/javascript" src="js/validador.js"></script>
<script type="text/javascript" src="js/image.js"></script>
<title>Ingreso Contacto</title>
</head>
<body>
	<form action="ServletAgregarCC" id="ServletAgregarCC" method="post"
		class="form-horizontal mitad" action="#">

		<div class="page-header">
			<h1>
				Ingreso de Contacto <small>Rellenar datos</small>
			</h1>
		</div>


		<div class="form-group">
			<label class="col-lg-3 control-label">Run</label>
			<div class="col-lg-3">
				<input type="text" class="form-control" name="run" required
					placeholder="Ingrese Run"> <br>
			</div>
		</div>

		<div class="form-group">
			<label class="col-lg-3 control-label">Nombre</label>
			<div class="col-lg-3">
				<input type="text" class="form-control" name="nombre" required
					placeholder="Ingrese Nombre"> <br>
			</div>
		</div>

		<div class="form-group">
			<label class="col-lg-3 control-label">Apellido</label>
			<div class="col-lg-3">
				<input type="text" class="form-control" name="apellido" required
					placeholder="Ingrese Apellido"> <br>
			</div>
		</div>

		<div class="form-group">
			<label class="col-lg-3 control-label">Mail</label>
			<div class="col-lg-3">
				<input type="text" class="form-control" name="mail" required
					placeholder="Ingrese Mail"> <br>
			</div>
		</div>

		<div class="form-group">
			<label class="col-lg-3 control-label">Telefono</label>
			<div class="col-lg-3">
				<input type="text" class="form-control" name="telefono" required
					placeholder="Ingrese Telefono"> <br>
			</div>
		</div>
		<div class="form-group">
			<label class="col-lg-3 control-label">Ciudad</label>
			<div class="col-lg-3">
				<input type="text" class="form-control" name="ciudad" required
					placeholder="Ingrese Ciudad"> <br>
			</div>
		</div>


		<div class="form-group">
			<label class="col-lg-3 control-label">Region</label>
			<div class="col-lg-3">
				<input type="text" class="form-control" name="region" required
					placeholder="Ingrese Region"> <br>
			</div>
		</div>

		<div class="form-group">
			<label class="col-lg-3 control-label">Pais</label>
			<div class="col-lg-3">
				<input type="text" class="form-control" name="pais" required
					placeholder="Ingrese Pais"> <br>
			</div>
		</div>

		<div class="form-group">
			<label class="col-lg-3 control-label">Empresa</label>
			<div class="col-lg-3">
				<select class="form-control" name="empresa" id="empresa">

					<option value="1">Pcfactory</option>

				</select> <br>
			</div>
		</div>





		<div class="form-group">
			<label for="foto">Foto Contacto:</label> <input id="inputImagen"
				name="inputImagen" type="file" multiple accept='image/*'
				onchange="encodeImage();" />
		</div>
		<div class="form-group">
			<textarea id="foto" name="foto" class="form-control textbox"
				style="display: none;"></textarea>
		</div>

		<div class="form-group" id="imgContainer"></div>

		<div class="form-group">
			<div class="col-lg-9 col-lg-offset-3">
				<button type="submit" class="btn btn-primary">Enviar</button>
			</div>
		</div>




	</form>


</body>

</html>