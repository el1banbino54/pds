package capanegocio;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.orm.PersistentException;
import org.orm.PersistentTransaction;

import capanegocio.*;
import servlet.ServletAgregarE;

public class Empresa {

	public Empresa() {

	}

	// arraylist de contactos
	private int uid;
	private String rut;
	private String nombre;
	private String direccion;
	private String telefono;
	private String mail;
	private String ciudad;
	private String region;
	private String pais;
	private String razonSocial;
	private String representanteLegal;
	private String rubro;

	/**
	 * 
	 * @return int de uid
	 */
	public int getUid() {
		return uid;
	}

	/**
	 * 
	 * @param uid
	 */
	public void setUid(int uid) {
		this.uid = uid;
	}

	/**
	 * 
	 * @return String de rut
	 */
	public String getRut() {
		return rut;
	}

	/**
	 * 
	 * @paramrut
	 */
	public void setRut(String rut) {
		this.rut = rut;
	}

	/**
	 * 
	 * @return String de direccion
	 */
	public String getDireccion() {
		return direccion;
	}

	/**
	 * 
	 * @param direccion
	 */
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	/**
	 * 
	 * @return String de telefono
	 */
	public String getTelefono() {
		return telefono;
	}

	/**
	 * 
	 * @param telefono
	 */
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	/**
	 * 
	 * @return String mail
	 */

	public String getMail() {
		return mail;
	}

	/**
	 * 
	 * @param mail
	 */
	public void setMail(String mail) {
		this.mail = mail;
	}

	/**
	 * 
	 * @return String ciudad
	 */
	public String getCiudad() {
		return ciudad;
	}

	/**
	 * 
	 * @param ciudad
	 */
	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	/**
	 * 
	 * @return String region
	 */
	public String getRegion() {
		return region;
	}

	/**
	 * 
	 * @param region
	 */
	public void setRegion(String region) {
		this.region = region;
	}

	/**
	 * 
	 * @return String pais
	 */

	public String getPais() {
		return pais;
	}

	/**
	 * 
	 * @param pais
	 */
	public void setPais(String pais) {
		this.pais = pais;
	}

	/**
	 * 
	 * @return String RazonSocial
	 */
	public String getRazonSocial() {
		return razonSocial;
	}

	/**
	 * 
	 * @param razonSocial
	 */
	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	/**
	 * 
	 * @return String representanteLegal
	 */
	public String getRepresentanteLegal() {
		return representanteLegal;
	}

	/**
	 * 
	 * @param representanteLegal
	 */
	public void setRepresentanteLegal(String representanteLegal) {
		this.representanteLegal = representanteLegal;
	}

	/**
	 * 
	 * @return String rubro
	 */
	public String getRubro() {
		return rubro;
	}

	/**
	 * 
	 * @param rubro
	 */
	public void setRubro(String rubro) {
		this.rubro = rubro;
	}

	/**
	 * 
	 * @return String nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * 
	 * @param nombre
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * M�todo ingresar permite registrar un contacto a la BD
	 * 
	 * @param contacto
	 * @return String que retorna un mensaje con la variable mensaje
	 * @throws PersistentException
	 */
	public static String ingresar(Empresa empresa) throws PersistentException {
		PersistentTransaction t = orm.Taller1MagisterInformaticaPersistentManager
				.instance().getSession().beginTransaction();
		String mensaje = "ingreso no exitoso de empresa";
		try {
			orm.Empresa lormEmpresa = orm.EmpresaDAO.createEmpresa();

			// Initialize the properties of the persistent object here
			try {
				lormEmpresa.setRut(empresa.getRut());
			} catch (NullPointerException e) {
				e.printStackTrace();
			}
			try {
				lormEmpresa.setNombre(empresa.getNombre());
			} catch (NullPointerException e) {
				e.printStackTrace();
			}
			try {
				lormEmpresa.setDireccion(empresa.getDireccion());
			} catch (NullPointerException e) {
				e.printStackTrace();
			}
			try {
				lormEmpresa.setTelefono(empresa.getTelefono());
			} catch (NullPointerException e) {
				e.printStackTrace();
			}
			try {
				lormEmpresa.setMail(empresa.getMail());
			} catch (NullPointerException e) {
				e.printStackTrace();
			}
			try {
				lormEmpresa.setCiudad(empresa.getCiudad());
			} catch (NullPointerException e) {
				e.printStackTrace();
			}
			try {
				lormEmpresa.setRegion(empresa.getRegion());
			} catch (NullPointerException e) {
				e.printStackTrace();
			}
			try {
				lormEmpresa.setPais(empresa.getPais());
			} catch (NullPointerException e) {
				e.printStackTrace();
			}
			try {
				lormEmpresa.setRazonSocial(empresa.getRazonSocial());
			} catch (NullPointerException e) {
				e.printStackTrace();
			}
			try {
				lormEmpresa.setRepresentanteLegal(empresa
						.getRepresentanteLegal());
			} catch (NullPointerException e) {
				e.printStackTrace();
			}
			try {
				lormEmpresa.setRubro(empresa.getRubro());
			} catch (NullPointerException e) {
				e.printStackTrace();
			}
			;

			mensaje = "Ingreso de empresa Exitosa";

			orm.EmpresaDAO.save(lormEmpresa);
			t.commit();
		} catch (Exception e) {
			t.rollback();
		}
		return mensaje;

	}

	/**
	 * M�todo actualizar permite actualizar un contacto en la BD
	 * 
	 * @param contacto
	 * @return string que retorna un mensaje con su variable mensaje
	 * @throws PersistentException
	 */
	public static String actualizar(Empresa empresa) throws PersistentException {
		PersistentTransaction t = orm.Taller1MagisterInformaticaPersistentManager
				.instance().getSession().beginTransaction();
		String mensaje = "actualizacion de empresa no exitosa";
		try {

			orm.Empresa lormEmpresa = orm.EmpresaDAO.loadEmpresaByORMID(empresa
					.getUid());

			// Update the properties of the persistent object

			try {
				lormEmpresa.setRut(empresa.getRut());
			} catch (NullPointerException e) {
				e.printStackTrace();
			}
			try {
				lormEmpresa.setNombre(empresa.getNombre());
			} catch (NullPointerException e) {
				e.printStackTrace();
			}
			try {
				lormEmpresa.setDireccion(empresa.getDireccion());
			} catch (NullPointerException e) {
				e.printStackTrace();
			}
			try {
				lormEmpresa.setTelefono(empresa.getTelefono());
			} catch (NullPointerException e) {
				e.printStackTrace();
			}
			try {
				lormEmpresa.setMail(empresa.getMail());
			} catch (NullPointerException e) {
				e.printStackTrace();
			}
			try {
				lormEmpresa.setCiudad(empresa.getCiudad());
			} catch (NullPointerException e) {
				e.printStackTrace();
			}
			try {
				lormEmpresa.setRegion(empresa.getRegion());
			} catch (NullPointerException e) {
				e.printStackTrace();
			}
			try {
				lormEmpresa.setPais(empresa.getPais());
			} catch (NullPointerException e) {
				e.printStackTrace();
			}
			try {
				lormEmpresa.setRazonSocial(empresa.getRazonSocial());

			} catch (NullPointerException e) {
				e.printStackTrace();
			}
			try {
				lormEmpresa.setRepresentanteLegal(empresa
						.getRepresentanteLegal());

			} catch (NullPointerException e) {
				e.printStackTrace();
			}
			try {
				lormEmpresa.setRubro(empresa.getRubro());
			} catch (NullPointerException e) {
				e.printStackTrace();
			}

			mensaje = "Actualizacion exitosa de empresa";

			orm.EmpresaDAO.save(lormEmpresa);
			t.commit();

		} catch (Exception e) {
			t.rollback();
		}
		return mensaje;

	}

	/**
	 * M�todo borrar permite eliminar un contacto de la BD
	 * 
	 * @param contacto
	 * @return String que retorna un mensaje con la variable mensaje
	 * @throws PersistentException
	 */
	public static String borrar(Empresa empresa) throws PersistentException {
		PersistentTransaction t = orm.Taller1MagisterInformaticaPersistentManager
				.instance().getSession().beginTransaction();
		String mensaje = "no borrado";
		try {

			orm.Empresa lormEmpresa = orm.EmpresaDAO.loadEmpresaByORMID(empresa
					.getUid());
			orm.EmpresaDAO.delete(lormEmpresa);
			// Delete the persistent object

			orm.EmpresaDAO.delete(lormEmpresa);
			t.commit();
			mensaje = "borrado";
			;
		} catch (Exception e) {
			t.rollback();
		}
		return mensaje;
	}

	/**
	 * M�todo listar() que lista las empresas almacenadas en la BD @
	 * 
	 * @return retorna una lista de empresas
	 * @throws PersistentException
	 */
	public static List<Empresa> listar() throws PersistentException {

		List<orm.Empresa> listaEmpresaOrm = orm.EmpresaDAO.queryEmpresa(null,
				null);
		List<Empresa> listaEmpresa = new ArrayList<>();

		for (orm.Empresa empresaOrm : listaEmpresaOrm) {
			Empresa empresa = new Empresa();

			empresa.setRut(empresaOrm.getRut());
			empresa.setNombre(empresaOrm.getNombre());
			empresa.setDireccion(empresaOrm.getDireccion());
			empresa.setTelefono(empresaOrm.getTelefono());
			empresa.setMail(empresaOrm.getMail());
			empresa.setCiudad(empresaOrm.getCiudad());
			empresa.setRegion(empresaOrm.getRegion());
			empresa.setPais(empresaOrm.getPais());
			empresa.setRazonSocial(empresaOrm.getRazonSocial());
			empresa.setRepresentanteLegal(empresaOrm.getRepresentanteLegal());
			empresa.setRubro(empresaOrm.getRubro());

			System.out.println("Listando");
			listaEmpresa.add(empresa);

		}

		return listaEmpresa;
	}

}