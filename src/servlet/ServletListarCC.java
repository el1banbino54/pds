package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.orm.PersistentException;

import capanegocio.Bitacora;
import capanegocio.Contacto;

/**
 * Servlet implementation class ServletListarCC
 */
public class ServletListarCC extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ServletListarCC() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	/**
	 * doGet permite el listar de los contactos
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	
		List<Contacto> lista = new ArrayList<>();
		// response.getWriter().append("Served at: ").append(request.getContextPath());
		try {
			lista = Contacto.listar();
			
			PrintWriter out = response.getWriter();
			out.println("Listando Contactos");
			out.println("");
		
			for (Contacto contacto : lista) {
				out.println("Uid: " + contacto.getUid());
				out.println("Run: " + contacto.getRun());
				out.println("Nombre: " + contacto.getNombre());
				out.println("Apellido: " + contacto.getApellido());
				out.println("Mail: " + contacto.getMail());
				out.println("Telefono: " + contacto.getTelefono());
				out.println("Pais: " + contacto.getPais());
				out.println("Region: " + contacto.getRegion());
				out.println("Ciudad: " + contacto.getCiudad());
			
				//out.println("Foto:" + contacto.getFoto());
				out.println("Empresa: " + contacto.getEmpresa().getNombre());
			
			
			}
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		request.setAttribute("contacto", lista);
		request.getRequestDispatcher("/FormularioListarContacto.jsp").forward(
				request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	
	}

	/**
	 * @see HttpServlet#doPut(HttpServletRequest, HttpServletResponse)
	 */
	protected void doPut(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doDelete(HttpServletRequest, HttpServletResponse)
	 */
	protected void doDelete(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
