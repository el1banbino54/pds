
    <%@ page language="java" contentType="text/html; charset=UTF-8"
  pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet"></link>
    <link href="//oss.maxcdn.com/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css" rel="stylesheet"></link>
    
    <script src="//oss.maxcdn.com/jquery/1.11.1/jquery.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <script src="//oss.maxcdn.com/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.min.js"></script>
       
<script type="text/javascript" src="./js/validador.js"></script>
<title>Login</title>
</head>

	<div class="page-header">
  <h1>Login
  <small>Rellenar datos</small>
  </h1>
</div>

<body>
	<div class="container">
		<h2>Ingrese a la plataforma</h2>
		<hr>	
		<form class="form-inline" action="ServletLogin" method="post" role="form">
			<div class="form-group">
				 <label class="sr-only" for="user">Usuario</label>
				 <input class="form-control" type="text" id="usuario" name="usuario" placeholder="Introduce tu Usuario" required>
			</div>
			<div class="form-group">
				 <label class="sr-only" for="password">Password</label>
				 <input class="form-control" type="password" id="password" name="password" placeholder="Introduce tu Password" required>
			</div>				
			
			<button type="submit" class="btn btn-success" value="login">Entrar</button>
		</form>		
		<h4 class="text-warning">${LoginStatus}</h4>		
	</div>
</body>

</html>