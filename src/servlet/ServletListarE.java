package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.orm.PersistentException;

import capanegocio.Contacto;
import capanegocio.Empresa;;

/**
 * Servlet implementation class ServletListarE
 */
public class ServletListarE extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletListarE() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
List<Empresa> lista = new ArrayList<>();
		
		String m="";
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		try {
			lista=Empresa.listar();
			PrintWriter out = response.getWriter();
			m="Listando Usuarios";
			out.println("Listando Usuarios");
			out.println("");
			
			for(Empresa empresa:lista){
				out.println("Id: " + empresa.getUid());
				out.println("Rut: " + empresa.getRut());
				out.println("Nombre: " + empresa.getNombre());
				out.println("Direccion: " + empresa.getDireccion());
				out.println("Telefono: " + empresa.getTelefono());
				out.println("Mail: " + empresa.getMail());
				out.println("Ciudad: " + empresa.getCiudad());
				out.println("Region: " + empresa.getRegion());
				out.println("Pais: " + empresa.getPais());
				out.println("Razon social: " + empresa.getRazonSocial());
				out.println("Representante Legal: " + empresa.getRepresentanteLegal());
				out.println("Rubro: " + empresa.getRubro());
			    out.println("");
		}
			} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		request.setAttribute("cont", lista);
		request.getRequestDispatcher("/FormularioListarContacto.jsp").forward(request, response);
	}
	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
